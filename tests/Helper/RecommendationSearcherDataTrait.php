<?php

namespace Tests\Helper;

use PHPUnit\Framework\MockObject\MockObject;
use stdClass;

trait RecommendationSearcherDataTrait
{
    private function mockIterator(MockObject $iteratorMock, array $items): void
    {
        $iteratorData = new stdClass();
        $iteratorData->array = $items;
        $iteratorData->position = 0;

        $iteratorMock
            ->method('rewind')
            ->willReturnCallback(function() use ($iteratorData) {
                $iteratorData->position = 0;
            });

        $iteratorMock
            ->method('current')
            ->willReturnCallback(static fn() => $iteratorData->array[$iteratorData->position]);

        $iteratorMock
            ->method('key')
            ->willReturnCallback(static fn() => $iteratorData->position);

        $iteratorMock
            ->method('next')
            ->willReturnCallback(function() use ($iteratorData) {
                $iteratorData->position++;
            });

        $iteratorMock
            ->method('valid')
            ->willReturnCallback(static fn() => isset($iteratorData->array[$iteratorData->position]));
    }
}
