<?php

declare(strict_types=1);

namespace Tests\Unit\UseCase;

use App\AppException;
use App\Application\AddNewMovie\DatabaseMovieCreator;
use App\Application\AddNewMovie\PageStorage;
use App\Application\AddNewMovie\PosterDownloader;
use App\Application\AddNewMovie\PosterStorage;
use App\Application\Exceptions\ParserException;
use App\Application\Parser\Crawler;
use App\Application\Parser\SchemaGetter;
use App\Application\Parser\SchemaValueObjectFactory;
use App\Application\Request\AddNewMovieRequest;
use App\Application\Telegram\DatabaseMovieManager;
use App\Application\UseCase\AddNewMovieUseCase;
use App\Domain\Models\Movie;
use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\MockObject\Exception;
use Symfony\Component\Validator\Validation;
use Tests\TestCase;

class AddNewMovieUserCaseTest extends TestCase
{
    /**
     * @throws \JsonException
     * @throws AppException
     * @throws ParserException
     * @throws \Throwable
     * @throws Exception
     * @throws GuzzleException
     */
    public function testAddingNewMovie(): void
    {
        $dummyUrl = 'https://www.imdb.com/title/tt000000/';
        $request = new AddNewMovieRequest($dummyUrl);
        $html = file_get_contents(__DIR__ . '/../../Fixtures/imdb.html');
        $pathToPoster = 'path/to/poster.jpg';

        $crawler = $this->createMock(Crawler::class);
        $crawler->expects($this->once())
            ->method('parse')
            ->with($dummyUrl)
            ->willReturn($html);

        $pageStorage = $this->createMock(PageStorage::class);
        $pageStorage->expects($this->once())
            ->method('exists')
            ->willReturn(false);
        $pageStorage->expects($this->once())
            ->method('store');

        $posterStorage = $this->createMock(PosterStorage::class);
        $posterStorage->expects($this->once())
            ->method('exists')
            ->willReturn(false);
        $posterStorage->expects($this->once())
            ->method('store');
        $posterStorage->expects($this->once())
            ->method('getPathToFile')
            ->willReturn($pathToPoster);

        $movie = Movie::factory()->make(['id' => 1]);

        $databaseMovieCreator = $this->createMock(DatabaseMovieCreator::class);
        $databaseMovieCreator->expects($this->once())
            ->method('create')
            ->willReturn($movie);

        $posterDownloader = $this->createMock(PosterDownloader::class);
        $posterDownloader->expects($this->once())
            ->method('download')
            ->willReturn($pathToPoster);

        $movieManager = $this->createMock(DatabaseMovieManager::class);
        $movieManager->expects($this->once())
            ->method('existsBySourceUrl')
            ->willReturn(false);

        $response = (new AddNewMovieUseCase(
            $crawler,
            Validation::createValidator(),
            $pageStorage,
            new SchemaGetter(
                new SchemaValueObjectFactory(),
                Validation::createValidator(),
            ),
            $posterStorage,
            $databaseMovieCreator,
            $posterDownloader,
            $movieManager
        ))->execute($request);

        $this->assertSame($movie->id, $response->movieId);
    }

    /**
     * @throws \JsonException
     * @throws AppException
     * @throws ParserException
     * @throws \Throwable
     * @throws Exception
     * @throws GuzzleException
     */
    public function testAddingAlreadyDownloadedMovie(): void
    {
        $dummyUrl = 'https://www.imdb.com/title/tt000000/';
        $request = new AddNewMovieRequest($dummyUrl);
        $html = file_get_contents(__DIR__ . '/../../Fixtures/imdb.html');
        $pathToPoster = 'path/to/poster.jpg';

        $crawler = $this->createMock(Crawler::class);
        $crawler->expects($this->never())
            ->method('parse');

        $pageStorage = $this->createMock(PageStorage::class);
        $pageStorage->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $pageStorage->expects($this->once())
            ->method('get')
            ->willReturn($html);
        $pageStorage->expects($this->never())
            ->method('store');

        $posterStorage = $this->createMock(PosterStorage::class);
        $posterStorage->expects($this->once())
            ->method('getPathToFile')
            ->willReturn($pathToPoster);
        $movieManager = $this->createMock(DatabaseMovieManager::class);
        $movieManager->expects($this->once())
            ->method('existsBySourceUrl')
            ->willReturn(false);

        $movie = Movie::factory()->make(['id' => 1]);
        $databaseMovieCreator = $this->createMock(DatabaseMovieCreator::class);
        $databaseMovieCreator->expects($this->once())
            ->method('create')
            ->willReturn($movie);
        $posterDownloader = $this->createMock(PosterDownloader::class);

        $response = (new AddNewMovieUseCase(
            $crawler,
            Validation::createValidator(),
            $pageStorage,
            new SchemaGetter(
                new SchemaValueObjectFactory(),
                Validation::createValidator(),
            ),
            $posterStorage,
            $databaseMovieCreator,
            $posterDownloader,
            $movieManager
        ))->execute($request);

        $this->assertSame($movie->id, $response->movieId);
    }

    /**
     * @throws \JsonException
     * @throws ParserException
     * @throws AppException
     * @throws \Throwable
     * @throws Exception
     * @throws GuzzleException
     */
    public function testNotAddingExistingPoster(): void
    {
        $dummyUrl = 'https://www.imdb.com/title/tt000000/';
        $request = new AddNewMovieRequest($dummyUrl);
        $html = file_get_contents(__DIR__ . '/../../Fixtures/imdb.html');
        $pathToPoster = 'path/to/poster.jpg';

        $crawler = $this->createMock(Crawler::class);

        $pageStorage = $this->createMock(PageStorage::class);
        $pageStorage->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $pageStorage->expects($this->once())
            ->method('get')
            ->willReturn($html);

        $posterStorage = $this->createMock(PosterStorage::class);
        $posterStorage->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $posterStorage->expects($this->never())
            ->method('store');
        $posterStorage->expects($this->once())
            ->method('getPathToFile')
            ->willReturn($pathToPoster);

        $movieManager = $this->createMock(DatabaseMovieManager::class);
        $movieManager->expects($this->once())
            ->method('existsBySourceUrl')
            ->willReturn(false);

        $movie = Movie::factory()->make(['id' => 1]);

        $databaseMovieCreator = $this->createMock(DatabaseMovieCreator::class);
        $databaseMovieCreator->expects($this->once())
            ->method('create')
            ->willReturn($movie);
        $posterDownloader = $this->createMock(PosterDownloader::class);

        $response = (new AddNewMovieUseCase(
            $crawler,
            Validation::createValidator(),
            $pageStorage,
            new SchemaGetter(
                new SchemaValueObjectFactory(),
                Validation::createValidator(),
            ),
            $posterStorage,
            $databaseMovieCreator,
            $posterDownloader,
            $movieManager
        ))->execute($request);

        $this->assertSame($movie->id, $response->movieId);
    }

    /**
     * @throws \JsonException
     * @throws AppException
     * @throws ParserException
     * @throws \Throwable
     * @throws Exception
     * @throws GuzzleException
     */
    public function testNotDownloadingMovieThatWeAlreadyHave(): void
    {
        $dummyUrl = 'https://www.imdb.com/title/tt000000/';
        $request = new AddNewMovieRequest($dummyUrl);
        $pathToPoster = 'path/to/poster.jpg';

        $crawler = $this->createMock(Crawler::class);

        $pageStorage = $this->createMock(PageStorage::class);
        $pageStorage->expects($this->never())
            ->method('exists');
        $pageStorage->expects($this->never())
            ->method('get');

        $posterStorage = $this->createMock(PosterStorage::class);
        $posterStorage->expects($this->never())
            ->method('exists');
        $posterStorage->expects($this->never())
            ->method('store');
        $posterStorage->expects($this->never())
            ->method('getPathToFile');

        $movie = Movie::factory()->make(['id' => 1]);
        $movieManager = $this->createMock(DatabaseMovieManager::class);
        $movieManager->expects($this->once())
            ->method('existsBySourceUrl')
            ->willReturn(true);
        $movieManager->expects($this->once())
            ->method('getBySourceUrl')
            ->willReturn($movie);

        $databaseMovieCreator = $this->createMock(DatabaseMovieCreator::class);
        $posterDownloader = $this->createMock(PosterDownloader::class);

        $response = (new AddNewMovieUseCase(
            $crawler,
            Validation::createValidator(),
            $pageStorage,
            new SchemaGetter(
                new SchemaValueObjectFactory(),
                Validation::createValidator(),
            ),
            $posterStorage,
            $databaseMovieCreator,
            $posterDownloader,
            $movieManager
        ))->execute($request);

        $this->assertSame($movie->id, $response->movieId);
    }
}
