<?php

namespace Tests\Unit;

use App\AppException;
use App\Application\RecommendationSystem\RecommendationFactory;
use App\Application\RecommendationSystem\RecommendationSearcher;
use App\Domain\UserRatingsIteratorInterface;
use App\Domain\ValueObject\RecommendationSystem\Movie;
use App\Domain\ValueObject\RecommendationSystem\MovieRating;
use App\Domain\ValueObject\RecommendationSystem\Rating;
use App\Domain\ValueObject\RecommendationSystem\Score;
use App\Domain\ValueObject\RecommendationSystem\TargetUser;
use App\Domain\ValueObject\RecommendationSystem\User;
use App\Domain\ValueObject\RecommendationSystem\UserRatings;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use Tests\Helper\RecommendationSearcherDataTrait;

class RecommendationSearcherTest extends TestCase
{
    use RecommendationSearcherDataTrait;

    /**
     * A basic unit test example.
     * @throws AppException
     * @throws Exception
     */
    public function testGettingRecommendations(): void
    {
        $targetUser = new TargetUser(1);
        $newMovies = [
            new Movie(5),
            new Movie(6),
            new Movie(7),
        ];
        $usersRatings = [
            new UserRatings(
                new User(1),
                [
                    new MovieRating(new Movie(1), new Rating(4)),
                    new MovieRating(new Movie(3), new Rating(5)),
                    new MovieRating(new Movie(4), new Rating(3)),
                ]
            ),
            new UserRatings(
                new User(2),
                [
                    new MovieRating(new Movie(1), new Rating(5)),
                    new MovieRating(new Movie(3), new Rating(3)),
                    new MovieRating(new Movie(4), new Rating(3)),
                    new MovieRating($newMovies[0], new Rating(5)),
                    new MovieRating($newMovies[1], new Rating(2)),
                    new MovieRating($newMovies[2], new Rating(4)),
                ]
            ),
        ];

        $usersRatingsMock = $this->createMock(UserRatingsIteratorInterface::class);
        $this->mockIterator($usersRatingsMock, $usersRatings);

        $result = $this->getBaseRecommendationSearcher()->search($usersRatingsMock, $targetUser);

        $this->assertCount(count($newMovies), $result);
        $this->assertEquals($newMovies[0], $result[0]->getMovie());
        $this->assertEquals($newMovies[2], $result[1]->getMovie());
        $this->assertEquals($newMovies[1], $result[2]->getMovie());
    }

    /**
     * @throws AppException
     * @throws Exception
     */
    public function testEmptyRecommendationsWhenThereAreNoOtherUsers(): void
    {
        $targetUser = new TargetUser(1);
        $usersRatings = [
            new UserRatings(
                new User(1),
                [
                    new MovieRating(new Movie(1), new Rating(4)),
                    new MovieRating(new Movie(3), new Rating(5)),
                    new MovieRating(new Movie(4), new Rating(3)),
                ]
            ),
        ];

        $usersRatingsMock = $this->createMock(UserRatingsIteratorInterface::class);
        $this->mockIterator($usersRatingsMock, $usersRatings);

        $result = $this->getBaseRecommendationSearcher()->search($usersRatingsMock, $targetUser);

        $this->assertCount(0, $result);
    }

    /**
     * @throws AppException|Exception
     */
    public function testGettingRecommendationsWithZeroScoreWhenNotMatchedUsers(): void
    {
        $targetUser = new TargetUser(1);
        $notMatchedMovies = [
            new Movie(5),
            new Movie(6),
            new Movie(7),
        ];
        $usersRatings = [
            new UserRatings(
                new User(1),
                [
                    new MovieRating(new Movie(1), new Rating(4)),
                    new MovieRating(new Movie(3), new Rating(5)),
                    new MovieRating(new Movie(4), new Rating(3)),
                ]
            ),
            new UserRatings(
                new User(2),
                [
                    new MovieRating($notMatchedMovies[0], new Rating(5)),
                    new MovieRating($notMatchedMovies[1], new Rating(2)),
                    new MovieRating($notMatchedMovies[2], new Rating(4)),
                ]
            ),
        ];

        $usersRatingsMock = $this->createMock(UserRatingsIteratorInterface::class);
        $this->mockIterator($usersRatingsMock, $usersRatings);

        $result = $this->getBaseRecommendationSearcher()->search($usersRatingsMock, $targetUser);

        $this->assertCount(count($notMatchedMovies), $result);

        $zeroScore = new Score(0);

        foreach ($result as $recommendation) {
            $this->assertSame($zeroScore->getScoreValue(), $recommendation->getScore()->getScoreValue());
        }
    }

    private function getBaseRecommendationSearcher(): RecommendationSearcher
    {
        return new RecommendationSearcher(new RecommendationFactory());
    }
}
