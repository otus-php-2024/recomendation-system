<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Domain\Models\Movie;
use App\Domain\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call([
            GenreSeeder::class,
            DirectorSeeder::class,
            ActorSeeder::class,
            MovieSeeder::class,
            UserSeeder::class,
            RatingSeeder::class,
        ]);
    }
}
