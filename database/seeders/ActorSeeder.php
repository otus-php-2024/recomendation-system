<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Domain\Models\Actor;
use Illuminate\Database\Seeder;

class ActorSeeder extends Seeder
{
    public function run(): void
    {
        Actor::factory(100)->create();
    }
}
