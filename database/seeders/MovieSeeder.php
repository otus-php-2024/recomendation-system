<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Domain\Models\Actor;
use App\Domain\Models\Director;
use App\Domain\Models\Genre;
use App\Domain\Models\Movie;
use Illuminate\Database\Seeder;
use Random\RandomException;

class MovieSeeder extends Seeder
{
    /**
     * @throws RandomException
     */
    public function run(): void
    {
        /** @var Movie[] $movies */
        $movies = Movie::factory(500)->create();
        $actors = Actor::all();
        $genres = Genre::all();
        $directors = Director::all();

        foreach ($movies as $movie) {
            $actorsSample = $actors->random(random_int(1, 3));
            $genresSample = $genres->random(random_int(1, 3));
            $director = $directors->random(random_int(1, 2));

            $movie->actors()->attach($actorsSample);
            $movie->genres()->attach($genresSample);
            $movie->directors()->attach($director);
        }
    }
}
