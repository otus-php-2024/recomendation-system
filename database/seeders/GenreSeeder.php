<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Domain\Models\Genre;
use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    public function run(): void
    {
        $genresDefinitions = [
            'Action', 'Adventure', 'Comedy', 'Crime', 'Drama', 'Fantasy', 'Historical', 'Horror', 'Mystery', 'Thriller',
            'Comedy', 'Romance', 'Science Fiction', 'Western'
        ];

        foreach ($genresDefinitions as $genresDefinition) {
            Genre::factory()->create(['genre' => $genresDefinition]);
        }
    }
}
