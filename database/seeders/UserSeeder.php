<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Domain\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run(): void
    {
        User::factory(300)->create();
    }
}
