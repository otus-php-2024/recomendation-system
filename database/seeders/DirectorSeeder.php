<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Domain\Models\Director;
use Illuminate\Database\Seeder;

class DirectorSeeder extends Seeder
{
    public function run(): void
    {
        Director::factory(50)->create();
    }
}
