<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Domain\Models\Movie;
use App\Domain\Models\Rating;
use App\Domain\Models\User;
use Illuminate\Database\Seeder;
use Random\RandomException;

class RatingSeeder extends Seeder
{
    /**
     * @throws RandomException
     */
    public function run(): void
    {
        $users = User::all();
        $movies = Movie::all();
        $maxMovies = (int) round($movies->count() * 0.8); // 80% of movies

        foreach ($users as $user) {
            $moviesSample = $movies->random(random_int(1, $maxMovies));
            $ratings = Rating::factory($moviesSample->count())->make([
                'user_id' => $user->id,
            ]);

            foreach ($ratings as $index => $rating) {
                $rating->movie_id = $moviesSample->get($index)->id;
            }

            Rating::insert($ratings->toArray());
        }
    }
}
