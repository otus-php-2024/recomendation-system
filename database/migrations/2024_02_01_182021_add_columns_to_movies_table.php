<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('movies', static function (Blueprint $table) {
            $table->renameColumn('movie', 'title');
            $table->string('poster_path');
            $table->text('description');
            $table->unsignedDouble('imdb_rating', 3, 1)->default(0);
            $table->string('keywords');
            $table->string('trailer_url');
            $table->string('source_url')->unique();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('movies', static function (Blueprint $table) {
            $table->renameColumn('title', 'movie');
            $table->dropColumn('poster_path');
            $table->dropColumn('description');
            $table->dropColumn('imdb_rating');
            $table->dropColumn('keywords');
            $table->dropColumn('trailer_url');
            $table->dropColumn('source_url');
        });
    }
};
