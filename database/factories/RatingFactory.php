<?php
declare(strict_types=1);

namespace Database\Factories;

use App\Domain\Models\Rating;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Rating>
 */
class RatingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'movie_id'=>$this->faker->numberBetween(1,3000),
            'user_id'=>$this->faker->numberBetween(1,10000000),
            'rating' => $this->faker->numberBetween(1,5),
            'created_at' => $this->faker->dateTimeBetween('2020-01-01','2024-01-01')->getTimestamp(),
            'updated_at' => $this->faker->dateTimeBetween('2020-01-01','2024-01-01')->getTimestamp(),
        ];
    }

    public function newModel(array $attributes = []): Rating
    {
        return new Rating($attributes);
    }
}
