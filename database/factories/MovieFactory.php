<?php
declare(strict_types=1);

namespace Database\Factories;

use App\Domain\Models\Movie;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Movie>
 */
class MovieFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $posterImageIndex = $this->faker->numberBetween(1, 7);
        return [
            'title' => $this->faker->words($this->faker->numberBetween(1, 4), true),
            'created_at' => $this->faker->dateTimeBetween('2020-01-01','2024-01-01')->getTimestamp(),
            'updated_at' => $this->faker->dateTimeBetween('2020-01-01','2024-01-01')->getTimestamp(),
            'poster_path' => "dummy/{$posterImageIndex}.png",
            'description' => $this->faker->text,
            'imdb_rating' => $this->faker->randomFloat(1, 1, 10),
            'keywords' => $this->faker->words($this->faker->numberBetween(3, 6), true),
            'trailer_url' => $this->faker->url,
            'source_url' => $this->faker->unique()->url,
            'published_date' => $this->faker->dateTimeBetween('1970-01-01','2025-01-01')->format('Y-m-d'),
        ];
    }

    public function newModel(array $attributes = []): Movie
    {
        return new Movie($attributes);
    }
}
