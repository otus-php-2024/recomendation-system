<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Domain\Models\Actor;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Actor>
 */
class ActorFactory extends Factory
{
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'url' => $this->faker->unique()->url,
            'created_at' => $this->faker->dateTimeBetween('2020-01-01','2024-01-01')->getTimestamp(),
            'updated_at' => $this->faker->dateTimeBetween('2020-01-01','2024-01-01')->getTimestamp(),
        ];
    }

    public function newModel(array $attributes = []): Actor
    {
        return new Actor($attributes);
    }
}
