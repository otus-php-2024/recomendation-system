<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Domain\Models\Genre;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Genre>
 */
class GenreFactory extends Factory
{
    public function definition(): array
    {
        return [
            'genre' => $this->faker->word(),
            'created_at' => $this->faker->dateTimeBetween('2020-01-01','2024-01-01')->getTimestamp(),
            'updated_at' => $this->faker->dateTimeBetween('2020-01-01','2024-01-01')->getTimestamp(),
        ];
    }

    public function newModel(array $attributes = []): Genre
    {
        return new Genre($attributes);
    }
}
