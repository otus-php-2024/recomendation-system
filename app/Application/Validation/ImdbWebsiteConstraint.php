<?php

declare(strict_types=1);

namespace App\Application\Validation;

use Symfony\Component\Validator\Constraint;

class ImdbWebsiteConstraint extends Constraint
{
    public string $message = 'The website "{{ value }}" is not a imdb website.';
    public string $mode = 'strict';
}
