<?php

declare(strict_types=1);

namespace App\Application\Validation;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class ImdbWebsiteConstraintValidator  extends ConstraintValidator
{

    public function validate(mixed $value, Constraint $constraint): void
    {
        if (! $constraint instanceof ImdbWebsiteConstraint) {
            throw new UnexpectedTypeException($constraint, ImdbWebsiteConstraint::class);
        }

        if (! is_string($value)) {
            throw new UnexpectedValueException($value, 'string');
        }

        if (preg_match('/^https:\/\/www.imdb.com\/title\/tt\d{1,8}\/$/', $value) !== 1) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }
    }
}
