<?php

declare(strict_types=1);

namespace App\Application\Response;

readonly class AddNewMovieResponse
{
    public function __construct(public int $movieId)
    {
    }
}
