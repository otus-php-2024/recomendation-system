<?php

declare(strict_types=1);

namespace App\Application\Response\Telegram;

readonly class AddMovieCommandResponse
{
    public function __construct(
        public int    $chatId,
        public string $message,
        public string $parseMode,
    ) {
    }

    public function toArray(): array
    {
        return [
            'chat_id' => $this->chatId,
            'text' => $this->message,
            'parse_mode' => $this->parseMode,
        ];
    }
}
