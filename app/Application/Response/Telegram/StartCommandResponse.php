<?php

declare(strict_types=1);

namespace App\Application\Response\Telegram;

readonly class StartCommandResponse
{
    public function __construct(
        public int    $chatId,
        public string $message,
    ) {
    }

    public function toArray(): array
    {
        return [
            'chat_id' => $this->chatId,
            'text' => $this->message,
        ];
    }
}
