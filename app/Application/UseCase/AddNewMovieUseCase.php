<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\AppException;
use App\Application\AddNewMovie\DatabaseMovieCreator;
use App\Application\AddNewMovie\PageStorage;
use App\Application\AddNewMovie\PosterDownloader;
use App\Application\AddNewMovie\PosterStorage;
use App\Application\Exceptions\ParserException;
use App\Application\Parser\Crawler;
use App\Application\Parser\SchemaGetter;
use App\Application\Request\AddNewMovieRequest;
use App\Application\Response\AddNewMovieResponse;
use App\Application\Telegram\DatabaseMovieManager;
use App\Application\Validation\ImdbWebsiteConstraint;
use App\Application\ViolationsExceptionTrait;
use App\Domain\ValueObject\AddNewMovie\MovieUrl;
use App\Domain\ValueObject\AddNewMovie\PosterStoragePath;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use LogicException;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Sequentially;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Throwable;

class AddNewMovieUseCase
{
    use ViolationsExceptionTrait;

    public function __construct(
        private readonly Crawler              $crawler,
        private readonly ValidatorInterface   $validator,
        private readonly PageStorage          $pageStorage,
        private readonly SchemaGetter         $schemaGetter,
        private readonly PosterStorage        $posterStorage,
        private readonly DatabaseMovieCreator $databaseMovieCreator,
        private readonly PosterDownloader     $posterDownloader,
        private readonly DatabaseMovieManager $databaseMovieManager
    ) {
    }

    /**
     * @throws GuzzleException
     * @throws AppException
     * @throws JsonException
     * @throws ParserException
     * @throws Throwable
     */
    public function execute(AddNewMovieRequest $request): AddNewMovieResponse
    {
        $this->validate($request);

        $url = new MovieUrl($request->url);

        if ($this->databaseMovieManager->existsBySourceUrl($url)) {
            $movie = $this->databaseMovieManager->getBySourceUrl($url);

            if ($movie === null) {
                throw new LogicException("Cannot find movie by url '{$url->getUrl()}' after checking in database");
            }

            return new AddNewMovieResponse($movie->id);
        }

        $pageFileName = $this->getPageFileName($request->url);

        if ($this->pageStorage->exists($pageFileName)) {
            $html = $this->pageStorage->get($pageFileName);
            $schema = $this->schemaGetter->get($html);
        } else {
            $html = $this->crawler->parse($request->url);
            $schema = $this->schemaGetter->get($html);

            $this->pageStorage->store($pageFileName, $html);
        }

        $posterFileName = $this->getPosterFileName($schema->getPoster()->getPoster());

        if (!$this->posterStorage->exists($posterFileName)) {
            $this->posterStorage->store($posterFileName, $this->posterDownloader->download($schema->getPoster()->getPoster()));
        }

        $posterStoragePath = $this->posterStorage->getPathToFile($posterFileName);

        $movie = $this->databaseMovieCreator->create(
            new MovieUrl($request->url),
            $schema,
            new PosterStoragePath($posterStoragePath)
        );

        return new AddNewMovieResponse($movie->id);
    }

    /**
     * @throws AppException
     */
    private function validate(AddNewMovieRequest $request): void
    {
        $input = [
            'url' => $request->url,
        ];
        $constraints = new Collection([
            'url' => new Sequentially([
                new NotBlank(),
                new Url(protocols: ['https']),
                new ImdbWebsiteConstraint(),
            ]),
        ]);

        $violations = $this->validator->validate($input, $constraints);

        if ($violations->count() > 0) {
            $this->throwViolationsException($violations);
        }
    }

    private function getPageFileName(string $url): string
    {
        return md5($url) . '.html';
    }

    private function getPosterFileName(string $url): string
    {
        if (preg_match('/\.(jpg|jpeg|png|gif|webp)$/', $url, $matches) === false) {
            throw new LogicException("Url '$url' does not have a valid image extension");
        }

        $extension = $matches[1];

        return md5($url) . '.' . $extension;
    }
}
