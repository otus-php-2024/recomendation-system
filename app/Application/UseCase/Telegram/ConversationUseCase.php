<?php

declare(strict_types=1);

namespace App\Application\UseCase\Telegram;

use App\AppException;
use App\Application\Exceptions\TelegramException as AppTelegramException;
use App\Application\Request\Telegram\AddMovieToQueueRequest;
use App\Application\Request\Telegram\ConversationRequest;
use App\Application\Telegram\DatabaseChatStatusManager;
use App\Application\ViolationsExceptionTrait;
use App\Domain\Models\ChatStatus;
use App\Domain\Telegram\ApiCallbacks;
use App\Domain\Telegram\ApiCommands;
use App\Domain\ValueObject\Telegram\ChatId;
use Illuminate\Support\Facades\App;
use JsonException;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Json;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Sequentially;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ConversationUseCase
{
    use ViolationsExceptionTrait;

    public function __construct(
        private readonly Telegram $telegram,
        private readonly ValidatorInterface $validator,
        private readonly DatabaseChatStatusManager $databaseChatStatusManager
    )
    {
    }

    /**
     * @throws AppException
     * @throws TelegramException
     * @throws AppTelegramException
     * @throws JsonException
     */
    public function execute(ConversationRequest $request): void
    {
        $this->validate($request);

        // commands run by command handlers
        if (!empty($request->botCommand)) {
            return;
        }

        if (!empty($request->callbackData)) {
            $this->telegram->executeCommand($this->getActionFromCallback($request));
            return;
        }

        $chatId = new ChatId($request->chatId);
        $status = $this->databaseChatStatusManager->get($chatId);

        if ($status !== null && $status->status === ChatStatus::STATUS_WAITING_FOR_FILM_URL) {
            /** @var AddMovieToQueueUseCase $service */
            $service = App::make(AddMovieToQueueUseCase::class);
            $dto = new AddMovieToQueueRequest($chatId->getChatId(), $request->message);

            $service->execute($dto);
            return;
        }

        $addFilmCommand = ApiCommands::ADD_FILM;
        $recommendCommand = ApiCommands::RECOMMEND_FILM;
        Request::sendMessage([
            'chat_id' => $chatId->getChatId(),
            'text' => "Unknown command or message. Please use one of the following commands: $addFilmCommand, $recommendCommand.",
        ]);
    }

    /**
     * @throws AppException
     * @throws JsonException
     */
    private function validate(ConversationRequest $request): void
    {
        $input = [
            'chatId' => $request->chatId,
            'dateTimestamp' => $request->dateTimestamp,
            'message' => $request->message,
            'botCommand' => $request->botCommand,
            'callbackData' => $request->callbackData,
        ];

        $constraints = new Collection([
            'chatId' => new Sequentially([
                new NotBlank(),
                new Positive(),
            ]),
            'dateTimestamp' => new Sequentially([
                new NotBlank(),
                new Positive(),
            ]),
            'message' => new Sequentially([
                new NotBlank(),
            ]),
            'botCommand' => new Sequentially([
                new Choice(['choices' => ApiCommands::COMMANDS]),
            ]),
            'callbackData' => new Sequentially([
                new Json()
            ])
        ]);

        $violations = $this->validator->validate($input, $constraints);

        if ($violations->count() > 0) {
            $this->throwViolationsException($violations);
        }

        if ($request->callbackData === null) {
            return;
        }

        $input = [
            'action' => $this->getActionFromCallback($request),
        ];
        $constraints = new Collection([
            'action' => new Sequentially([
                new NotBlank(),
                new Choice(['choices' => ApiCallbacks::CALLBACKS]),
            ]),
        ]);

        $violations = $this->validator->validate($input, $constraints);

        if ($violations->count() > 0) {
            $this->throwViolationsException($violations);
        }
    }

    /**
     * @throws JsonException
     */
    private function getActionFromCallback(ConversationRequest $request): string
    {
        return json_decode($request->callbackData, true, 512, JSON_THROW_ON_ERROR)['action'];
    }
}
