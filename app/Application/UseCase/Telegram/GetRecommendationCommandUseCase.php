<?php

declare(strict_types=1);

namespace App\Application\UseCase\Telegram;

use App\AppException;
use App\Application\Exceptions\TelegramException;
use App\Application\Request\Telegram\GetListOfRecommendationsRequest;
use App\Application\Request\Telegram\GetRecommendationCommandRequest;
use App\Application\Telegram\Factory\PublisherMakeRecommendationsQueueMessageFactory;
use App\Application\Telegram\MessageSender;
use App\Application\Telegram\RecommendationCache;
use App\Application\ViolationsExceptionTrait;
use App\Domain\ValueObject\Queue\PublisherMakeRecommendationsRequest;
use App\Domain\ValueObject\Telegram\ChatId;
use App\Domain\ValueObject\Telegram\SendMessageText;
use Exception;
use Illuminate\Support\Facades\App;
use JsonException;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GetRecommendationCommandUseCase
{
    use ViolationsExceptionTrait;

    public function __construct(
        private readonly ValidatorInterface                              $validator,
        private readonly RecommendationCache                             $recommendationCache,
        private readonly AMQPStreamConnection                            $rabbitConnection,
        private readonly PublisherMakeRecommendationsQueueMessageFactory $publisherMakeRecommendationsQueueMessageFactory,
        private readonly MessageSender                                   $messageSender
    )
    {
    }

    /**
     * @throws AppException
     * @throws TelegramException
     * @throws \Longman\TelegramBot\Exception\TelegramException
     * @throws JsonException
     * @throws Exception
     */
    public function execute(GetRecommendationCommandRequest $request): void
    {
        $this->validate($request);

        $chatId = new ChatId((int) $request->chatId);

        if ($this->recommendationCache->has($chatId)) {
            /** @var GetListOfRecommendationsUseCase $recommendationsUseCase */
            $recommendationsUseCase = App::make(GetListOfRecommendationsUseCase::class);
            $dto = new GetListOfRecommendationsRequest($chatId->getChatId());
            $recommendationsUseCase->execute($dto);

            return;
        }

        $channel = $this->rabbitConnection->channel();
        $queueRequest = new PublisherMakeRecommendationsRequest($chatId);
        $queueMessage = $this->publisherMakeRecommendationsQueueMessageFactory->create($queueRequest);

        $channel->basic_publish($queueMessage, '', 'make_recommendations_queue');

        $channel->close();
        $this->rabbitConnection->close();

        $responseMessage = 'I am looking for the best movies for you. Please wait a few seconds';

        $this->messageSender->send($chatId, new SendMessageText($responseMessage));
    }

    /**
     * @throws AppException
     */
    private function validate(GetRecommendationCommandRequest $request): void
    {
        $input = [
            'chatId' => $request->chatId,
        ];
        $constraints = new Collection([
            'chatId' => [
                new NotBlank(),
                new Positive(),
            ],
        ]);
        $violations = $this->validator->validate($input, $constraints);

        if (count($violations) > 0) {
            $this->throwViolationsException($violations);
        }
    }
}
