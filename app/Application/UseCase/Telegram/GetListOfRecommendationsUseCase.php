<?php

declare(strict_types=1);

namespace App\Application\UseCase\Telegram;

use App\AppException;
use App\Application\Exceptions\TelegramException;
use App\Application\MainStorage;
use App\Application\Request\Telegram\GetListOfRecommendationsRequest;
use App\Application\Telegram\DatabaseMovieManager;
use App\Application\Telegram\MessageSender;
use App\Application\Telegram\MovieCardGetter;
use App\Application\Telegram\RecommendationCache;
use App\Application\ViolationsExceptionTrait;
use App\Domain\Telegram\ApiCallbacks;
use App\Domain\Telegram\ApiCommands;
use App\Domain\ValueObject\RecommendationSystem\Movie;
use App\Domain\ValueObject\Telegram\ChatId;
use App\Domain\ValueObject\Telegram\InlineButton;
use App\Domain\ValueObject\Telegram\PhotoPath;
use App\Domain\ValueObject\Telegram\SendMessageText;
use JsonException;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GetListOfRecommendationsUseCase
{
    use ViolationsExceptionTrait;

    private const int MAX_RECOMMENDATIONS = 5;

    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly RecommendationCache $recommendationCache,
        private readonly MessageSender $messageSender,
        private readonly DatabaseMovieManager $databaseMovieManager,
        private readonly MovieCardGetter $movieCardGetter,
        private readonly MainStorage $mainStorage
    ) {
    }

    /**
     * @throws AppException
     * @throws TelegramException
     * @throws \Longman\TelegramBot\Exception\TelegramException
     * @throws JsonException
     */
    public function execute(GetListOfRecommendationsRequest $request): void
    {
        $this->validate($request);

        $chatId = new ChatId($request->chatId);

        if (!$this->recommendationCache->has($chatId)) {
            $this->messageSender->send(
                $chatId,
                new SendMessageText(
                    'Recommendations need to be refreshed. Please click' . ApiCommands::RECOMMEND_FILM . ' button to refresh recommendations.')
            );
            return;
        }

        $recommendations = $this->recommendationCache->get($chatId);

        foreach (array_slice($recommendations, $request->offset, self::MAX_RECOMMENDATIONS) as $recommendation) {
            /* @var $recommendation Movie */
            $movie = $this->databaseMovieManager->getById($recommendation);

            if ($movie === null) {
                throw TelegramException::movieNotFoundInDatabase($recommendation->getMovieId());
            }

            $movieCard = $this->movieCardGetter->getMarkdown($movie);

            $this->messageSender->sendPoster(
                $chatId,
                new SendMessageText($movieCard->getMovieCard(), SendMessageText::PARSE_MODE_MARKDOWN),
                new PhotoPath($this->mainStorage->getAppFullPath($movieCard->getPosterPath()->getPath())),
                $this->getButtons($recommendation)
            );
        }

        $nextOffset = $request->offset + self::MAX_RECOMMENDATIONS;

        $this->messageSender->sendMessageWithInlineButtons(
            $chatId,
            new SendMessageText('If you want move, press the button bellow'),
            [
                new InlineButton(
                    'Show more',
                    json_encode(['action' => ApiCallbacks::SHOW_MORE, 'offset' => $nextOffset], JSON_THROW_ON_ERROR)
                )
            ]
        );
    }

    /**
     * @throws AppException
     */
    private function validate(GetListOfRecommendationsRequest $request): void
    {
        $input = [
            'chatId' => $request->chatId,
            'offset' => $request->offset,
        ];
        $constraints = new Collection([
            'chatId' => [
                new NotBlank(),
                new Positive(),
            ],
            'offset' => [
                new NotBlank(),
                new Range(['min' => 0])
            ]
        ]);
        $violations = $this->validator->validate($input, $constraints);

        if ($violations->count() > 0) {
            $this->throwViolationsException($violations);
        }
    }

    /**
     * @throws JsonException
     * @return InlineButton[]
     */
    private function getButtons(Movie $movie): array
    {
        return [
            new InlineButton(
                '1',
                json_encode(['action' => ApiCallbacks::RATE, 'movie' => $movie->getMovieId(), 'rating' => 1], JSON_THROW_ON_ERROR)
            ),
            new InlineButton(
                '2',
                json_encode(['action' => ApiCallbacks::RATE, 'movie' => $movie->getMovieId(), 'rating' => 2], JSON_THROW_ON_ERROR)
            ),
            new InlineButton(
                '3',
                json_encode(['action' => ApiCallbacks::RATE, 'movie' => $movie->getMovieId(), 'rating' => 3], JSON_THROW_ON_ERROR)
            ),
            new InlineButton(
                '4',
                json_encode(['action' => ApiCallbacks::RATE, 'movie' => $movie->getMovieId(), 'rating' => 4], JSON_THROW_ON_ERROR)
            ),
            new InlineButton(
                '5',
                json_encode(['action' => ApiCallbacks::RATE, 'movie' => $movie->getMovieId(), 'rating' => 5], JSON_THROW_ON_ERROR)
            ),
        ];
    }
}
