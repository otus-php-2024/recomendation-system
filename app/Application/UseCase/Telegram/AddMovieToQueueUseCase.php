<?php

declare(strict_types=1);

namespace App\Application\UseCase\Telegram;

use App\AppException;
use App\Application\Exceptions\TelegramException as AppTelegramException;
use App\Application\Request\Telegram\AddMovieToQueueRequest;
use App\Application\Telegram\DatabaseChatStatusManager;
use App\Application\Telegram\DatabaseMovieManager;
use App\Application\Telegram\Factory\PublisherAddMovieQueueMessageFactory;
use App\Application\Telegram\Factory\PublisherAfterAddMovieRequestFactory;
use App\Application\Telegram\MessageSender;
use App\Application\Validation\ImdbWebsiteConstraint;
use App\Application\ViolationsExceptionTrait;
use App\Domain\Models\ChatStatus as ChatStatusModel;
use App\Domain\ValueObject\Telegram\ChatId;
use App\Domain\ValueObject\Telegram\ChatStatus;
use App\Domain\ValueObject\Telegram\SendMessageText;
use Exception;
use JsonException;
use Longman\TelegramBot\Exception\TelegramException;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Sequentially;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AddMovieToQueueUseCase
{
    use ViolationsExceptionTrait;

    public function __construct(
        private readonly ValidatorInterface                   $validator,
        private readonly DatabaseChatStatusManager            $databaseChatStatusManager,
        private readonly MessageSender                        $messageSender,
        private readonly AMQPStreamConnection                 $rabbitConnection,
        private readonly PublisherAfterAddMovieRequestFactory $publisherAddMovieRequestFactory,
        private readonly PublisherAddMovieQueueMessageFactory $publisherAddMovieQueueMessageFactory,
        private readonly DatabaseMovieManager                 $databaseMovieManager
    )
    {
    }

    /**
     * @param AddMovieToQueueRequest $request
     * @throws TelegramException
     * @throws AppTelegramException
     * @throws AppException
     * @throws JsonException
     * @throws Exception
     */
    public function execute(AddMovieToQueueRequest $request): void
    {
        try {
            $this->validate($request);
        } catch (AppException $e) {
            if ($this->hasUrlValidationError($e)) {
                $errorMessage = 'Please, send me a valid IMDb link';
                $this->messageSender->send(new ChatId($request->chatId), new SendMessageText($errorMessage));
                return;
            }
        }

        $publisherRequest = $this->publisherAddMovieRequestFactory->create($request->chatId, $request->url);

        if ($this->databaseMovieManager->existsBySourceUrl($publisherRequest->getMovieUrl())) {
            // todo response with move card
            $userResponseMessage = <<<MESSAGE
            The movie already exists in the database. Thank you for your help anyway!
            MESSAGE;

            $this->messageSender->send($publisherRequest->getChatId(), new SendMessageText($userResponseMessage));
            return;
        }

        $channel = $this->rabbitConnection->channel();
        $queueMessage = $this->publisherAddMovieQueueMessageFactory->create($publisherRequest);

        $channel->basic_publish($queueMessage, '', 'add_movie_queue');

        $channel->close();
        $this->rabbitConnection->close();

        $this->databaseChatStatusManager->set(
            $publisherRequest->getChatId(),
            new ChatStatus(ChatStatusModel::STATUS_ADDING_FILM)
        );

        $userResponseMessage = <<<MESSAGE
        Thank you! I will check the movie and add it to my database. Then I will notify you about the result.
        MESSAGE;

        $this->messageSender->send($publisherRequest->getChatId(), new SendMessageText($userResponseMessage));
    }

    /**
     * @throws AppException
     */
    private function validate(AddMovieToQueueRequest $request): void
    {
        $input = [
            'chat_id' => $request->chatId,
            'url' => $request->url,
        ];
        $constraints = new Collection([
            'chat_id' => new Sequentially([
                new NotBlank(),
                new Positive(),
            ]),
            'url' => new Sequentially([
                new NotBlank(),
                new Url(protocols: ['https']),
                new ImdbWebsiteConstraint(),
            ]),
        ]);

        $violations = $this->validator->validate($input, $constraints);

        if ($violations->count() > 0) {
            $this->throwViolationsException($violations);
        }
    }

    private function hasUrlValidationError(AppException $exception): bool
    {
        if ($exception->getCode() !== AppException::VALIDATION_ERROR) {
            return false;
        }

        foreach ($exception->getData() as $violation) {
            if ($violation['property'] === '[url]') {
                return true;
            }
        }

        return false;
    }
}
