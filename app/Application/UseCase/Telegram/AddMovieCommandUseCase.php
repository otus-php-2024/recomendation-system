<?php

declare(strict_types=1);

namespace App\Application\UseCase\Telegram;

use App\AppException;
use App\Application\Exceptions\TelegramException;
use App\Application\Request\Telegram\AddMovieCommandRequest;
use App\Application\Response\Telegram\AddMovieCommandResponse;
use App\Application\Telegram\DatabaseChatStatusManager;
use App\Application\ViolationsExceptionTrait;
use App\Domain\Models\ChatStatus as ChatStatusModel;
use App\Domain\ValueObject\Telegram\ChatId;
use App\Domain\ValueObject\Telegram\ChatStatus;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Sequentially;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AddMovieCommandUseCase
{
    use ViolationsExceptionTrait;

    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly DatabaseChatStatusManager $databaseChatStatusManager
    )
    {
    }

    /**
     * @throws AppException
     * @throws TelegramException
     */
    public function execute(AddMovieCommandRequest $request): AddMovieCommandResponse
    {
        $this->validate($request);

        $this->databaseChatStatusManager->set(
            new ChatId($request->chatId),
            new ChatStatus( ChatStatusModel::STATUS_WAITING_FOR_FILM_URL)
        );

        $message = <<<TEXT
        Please, send me a link to the film you want to add to the database.
        Use only imdb.com links. It should look like this: `https://www.imdb.com/title/tt0111161/`
        TEXT;

        return new AddMovieCommandResponse($request->chatId, $message, 'Markdown');
    }

    /**
     * @throws AppException
     */
    private function validate(AddMovieCommandRequest $request): void
    {
        $input = [
            'chatId' => $request->chatId,
        ];
        $constraints = new Collection([
            'chatId' => new Sequentially([
                new NotBlank(),
                new Positive(),
            ]),
        ]);
        $violations = $this->validator->validate($input, $constraints);

        if (count($violations) > 0) {
            $this->throwViolationsException($violations);
        }
    }
}
