<?php

declare(strict_types=1);

namespace App\Application\UseCase\Telegram;

use App\AppException;
use App\Application\Request\Telegram\StartCommandRequest;
use App\Application\Response\Telegram\StartCommandResponse;
use App\Application\Telegram\Start\DatabaseChatCreator;
use App\Application\ViolationsExceptionTrait;
use App\Domain\Telegram\ApiCommands;
use App\Domain\ValueObject\Telegram\ChatId;
use App\Domain\ValueObject\Telegram\UserId;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Sequentially;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Throwable;

class StartCommandUseCase
{
    use ViolationsExceptionTrait;

    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly DatabaseChatCreator $databaseUserCreator
    )
    {
    }

    /**
     * @throws Throwable
     */
    public function execute(StartCommandRequest $request): StartCommandResponse
    {
        $this->validate($request);

        $this->databaseUserCreator->create(new UserId($request->userId), new ChatId($request->chatId));

        $addFilmCommand = ApiCommands::ADD_FILM;
        $recommendCommand = ApiCommands::RECOMMEND_FILM;
        $sendMessage = <<<EOT
        Hello! I am a bot that can help you to find the best film for you.

        You can use the following commands:
        $addFilmCommand - to add a new film to my database
        $recommendCommand - to get a recommendation for a film to watch
        EOT;

        return new StartCommandResponse($request->chatId, $sendMessage);
    }

    /**
     * @throws AppException
     */
    private function validate(StartCommandRequest $request): void
    {
        $arrayRequest = [
            'userId' => $request->userId,
            'chatId' => $request->chatId,
        ];
        $constraints = new Collection([
            'userId' => new Sequentially([
                new NotBlank(),
                new Positive(),
            ]),
            'chatId' => new Sequentially([
                new NotBlank(),
                new Positive(),
            ]),
        ]);
        $violations = $this->validator->validate($arrayRequest, $constraints);

        if (count($violations) > 0) {
            $this->throwViolationsException($violations);
        }
    }
}
