<?php

declare(strict_types=1);

namespace App\Application\UseCase\Telegram;

use App\AppException;
use App\Application\Exceptions\TelegramException;
use App\Application\Request\Telegram\RateMovieRequest;
use App\Application\Telegram\DatabaseMovieManager;
use App\Application\Telegram\DatabaseRatingManager;
use App\Application\Telegram\DatabaseUserManager;
use App\Application\Telegram\MessageSender;
use App\Application\ViolationsExceptionTrait;
use App\Domain\ValueObject\RecommendationSystem\Movie;
use App\Domain\ValueObject\RecommendationSystem\Rating;
use App\Domain\ValueObject\RecommendationSystem\User;
use App\Domain\ValueObject\Telegram\ChatId;
use App\Domain\ValueObject\Telegram\SendMessageText;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Sequentially;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RateMovieUseCase
{
    use ViolationsExceptionTrait;

    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly DatabaseMovieManager $databaseMovieManager,
        private readonly MessageSender $messageSender,
        private readonly DatabaseUserManager $databaseUserManager,
        private readonly DatabaseRatingManager $databaseRatingManager
    )
    {
    }

    /**
     * @param RateMovieRequest $request
     * @throws AppException
     * @throws TelegramException
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute(RateMovieRequest $request): void
    {
        $this->validate($request);

        $chatId = new ChatId($request->chatId);
        $movie = $this->databaseMovieManager->getById(new Movie($request->movieId));

        if ($movie === null) {
            $this->messageSender->send(
                $chatId,
                new SendMessageText("Something went wrong. I can't find the movie to rate.")
            );

            return;
        }

        $user = $this->databaseUserManager->getByChatId($chatId);

        if ($user === null) {
            throw AppException::userNotFoundByChatId($chatId->getChatId());
        }

        $this->databaseRatingManager->rate(
            new User($user->id),
            new Movie($movie->id),
            new Rating($request->rating)
        );

        $this->messageSender->send(
            $chatId,
            new SendMessageText("Thank you for rating the movie!")
        );
    }

    /**
     * @throws AppException
     */
    private function validate(RateMovieRequest $request)
    {
        $input = [
            'chatId' => $request->chatId,
            'movieId' => $request->movieId,
            'rating' => $request->rating,
        ];
        $constraints = new Collection([
            'chatId' => new Sequentially([
                new NotBlank(),
                new Positive(),
            ]),
            'movieId' => new Sequentially([
                new NotBlank(),
                new Positive(),
            ]),
            'rating' => new Sequentially([
                new NotBlank(),
                new Positive(),
                new Range([
                    'min' => 1,
                    'max' => 5,
                ])
            ]),
        ]);

        $violations = $this->validator->validate($input, $constraints);

        if ($violations->count() > 0) {
            $this->throwViolationsException($violations);
        }
    }
}
