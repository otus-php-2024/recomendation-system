<?php

declare(strict_types=1);

namespace App\Application\UseCase\Telegram;

use App\AppException;
use App\Application\Exceptions\TelegramException;
use App\Application\Request\Telegram\ConversationRequest;
use JsonException;
use Laravel\Prompts\Output\ConsoleOutput;
use LogicException;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Telegram;

readonly class GetUpdatesUseCase
{
    private const int SLEEP_TIME = 1;

    public function __construct(
        private Telegram            $telegram,
        private ConversationUseCase $telegramConversationUseCase,
        private ConsoleOutput       $output,
    )
    {
    }

    /**
     * @throws TelegramException
     * @throws AppException
     * @throws JsonException
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute(): void
    {
        $this->output->writeln('Start getting updates');

        while (true) {
            $response = $this->telegram->handleGetUpdates();

            if (!$response->isOk()) {
                $this->output->writeln('Error: ' . $response->printError(true));

                throw TelegramException::getUpdatesFailed($response->printError(true));
            }

            $updates = $response->getResult();

            if (empty($updates)) {
                $this->output->writeln('No updates');

                continue;
            }

            $this->output->writeln('Json response: ');
            print_r($response->toJson());
            $this->output->writeln('');

            foreach ($updates as $update) {
                if (!$update instanceof Update) {
                    throw new LogicException('Update is not instance of Update telegram class');
                }

                $dto = $update->getCallbackQuery() === null ?
                    $this->getMessageDTO($update) :
                    $this->getCallbackQueryDTO($update);
                $this->telegramConversationUseCase->execute($dto);
            }

            sleep(self::SLEEP_TIME);
        }
    }

    private function getMessageDTO(Update $update): ConversationRequest
    {
        $hasEntities = !empty($update->getMessage()->getEntities());

        return new ConversationRequest(
            $update->getMessage()->getChat()->getId(),
            $update->getMessage()->getDate(),
            $update->getMessage()->getText(),
            $hasEntities && $update->getMessage()->getEntities()[0]->getType() === 'bot_command' ?
                $update->getMessage()->getText() :
                null
        );
    }

    private function getCallbackQueryDTO(Update $update): ConversationRequest
    {
        return new ConversationRequest(
            $update->getCallbackQuery()->getMessage()->getChat()->getId(),
            $update->getCallbackQuery()->getMessage()->getDate(),
            $update->getCallbackQuery()->getMessage()->getText() ?? 'Empty message',
            null,
            $update->getCallbackQuery()->getData()
        );
    }
}
