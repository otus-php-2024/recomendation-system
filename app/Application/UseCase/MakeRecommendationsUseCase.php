<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\AppException;
use App\Application\Exceptions\TelegramException;
use App\Application\RecommendationSystem\ConvertRecommendationsToResultTrait;
use App\Application\RecommendationSystem\RecommendationSearcher;
use App\Application\RecommendationSystem\UsersRatingsIteratorFactory;
use App\Application\Request\MakeRecommendationsRequest;
use App\Application\Telegram\DatabaseMovieManager;
use App\Application\Telegram\DatabaseUserManager;
use App\Application\Telegram\RecommendationCache;
use App\Application\ViolationsExceptionTrait;
use App\Domain\ValueObject\RecommendationSystem\Recommendation;
use App\Domain\ValueObject\RecommendationSystem\TargetUser;
use App\Domain\ValueObject\Telegram\ChatId;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Sequentially;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class MakeRecommendationsUseCase
{
    use ViolationsExceptionTrait, ConvertRecommendationsToResultTrait;

    public function __construct(
        private readonly RecommendationCache          $recommendationCache,
        private readonly RecommendationSearcher       $recommendationSearcher,
        private readonly UsersRatingsIteratorFactory  $usersRatingsGeneratorFactory,
        private readonly DatabaseUserManager          $databaseUserManager,
        private readonly ValidatorInterface           $validator,
        private readonly DatabaseMovieManager         $databaseMovieManager
    )
    {
    }

    /**
     * @throws AppException
     * @throws TelegramException
     */
    public function execute(MakeRecommendationsRequest $request): void
    {
        $this->validate($request);

        $chatId = new ChatId($request->chatId);
        $user = $this->databaseUserManager->getByChatId($chatId);

        if ($user === null) {
            throw AppException::userNotFoundByChatId($chatId->getChatId());
        }

        $targetUser = new TargetUser($user->id);
        $recommendations = $this->getBaseAlgoRecommendations($targetUser);

        echo "Sorting\n";
        $this->sortMoviesWithoutRating($recommendations);
        $expire = 60 * 60 * 24 * 7; // 1 week
        echo "Setting cache\n";
        $this->recommendationCache->set($chatId, $recommendations, $expire);
    }

    private function validate(MakeRecommendationsRequest $request): void
    {
        $input = [
            'chatId' => $request->chatId,
        ];
        $constraints = new Collection([
            'chatId' => new Sequentially([
                new NotBlank(),
                new Positive(),
            ])
        ]);

        $this->validator->validate($input, $constraints);
    }

    /**
     * @param TargetUser $targetUser
     * @return Recommendation[]
     * @throws AppException
     */
    private function getBaseAlgoRecommendations(TargetUser $targetUser): array
    {
        $recommendations = $this->recommendationSearcher->search(
            $this->usersRatingsGeneratorFactory->create(),
            $targetUser
        );

        $addedMoviesIds = $this->getAddedMoviesIds($recommendations);
        $moviesNotAdded = $this->databaseMovieManager->getNotInIds($addedMoviesIds);
        $newRecommendations = [];

        foreach ($moviesNotAdded as $movie) {
            $newRecommendations[$movie->id] = 0;
        }

        array_push($recommendations, ...$this->convertRecommendationsToValueObjects($newRecommendations));

        return $recommendations;
    }

    private function getAddedMoviesIds(array $recommendations): array
    {
        $addedMoviesIds = [];

        foreach ($recommendations as $recommendation) {
            $addedMoviesIds[] = $recommendation->getMovie()->getMovieId();
        }

        return $addedMoviesIds;
    }

    /**
     * @param Recommendation[] $recommendations
     */
    private function sortMoviesWithoutRating(array &$recommendations): void
    {
        $moviesWithoutRating = $this->getMoviesWithoutRating($recommendations);
        $databaseMovies = $this->databaseMovieManager->getByIds($moviesWithoutRating);

        usort($recommendations, static function (Recommendation $a, Recommendation $b) use ($databaseMovies) {
            if ($a->getScore()->getScoreValue() === 0.0) {
                $scoreA = array_key_exists($a->getMovie()->getMovieId(), $databaseMovies)
                    ? $databaseMovies[$a->getMovie()->getMovieId()]->imdb_rating
                    : 0.0;
            } else {
                $scoreA = $a->getScore()->getScoreValue();
            }

            if ($b->getScore()->getScoreValue() === 0.0) {
                $scoreB = array_key_exists($b->getMovie()->getMovieId(), $databaseMovies)
                    ? $databaseMovies[$b->getMovie()->getMovieId()]->imdb_rating
                    : 0.0;
            } else {
                $scoreB = $b->getScore()->getScoreValue();
            }

            return $scoreB <=> $scoreA;
        });
    }

    /**
     * @param Recommendation[] $recommendations
     * @return int[]
     */
    private function getMoviesWithoutRating(array $recommendations): array
    {
        $moviesWithoutRating = [];

        foreach ($recommendations as $recommendation) {
            if ($recommendation->getScore()->getScoreValue() === 0.0) {
                $moviesWithoutRating[] = $recommendation->getMovie()->getMovieId();
            }
        }

        return $moviesWithoutRating;
    }
}
