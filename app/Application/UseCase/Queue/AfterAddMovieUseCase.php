<?php

declare(strict_types=1);

namespace App\Application\UseCase\Queue;

use App\AppException;
use App\Application\Telegram\DatabaseMovieManager;
use App\Application\Telegram\Factory\ConsumerAfterAddMovieRequestFactory;
use App\Application\Telegram\MessageSender;
use App\Domain\ValueObject\Telegram\SendMessageText;
use ErrorException;
use Laravel\Prompts\Output\ConsoleOutput;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

readonly class AfterAddMovieUseCase
{
    public function __construct(
        private AMQPStreamConnection                $connection,
        private ConsumerAfterAddMovieRequestFactory $consumerAfterAddMovieRequestFactory,
        private ConsoleOutput                                $output,
        private MessageSender                                $messageSender,
        private DatabaseMovieManager                         $databaseMovieManager,
    )
    {
    }

    /**
     * @throws ErrorException
     */
    public function execute(): void
    {
        $receiveChannel = $this->connection->channel();

        $receiveChannel->queue_declare('after_add_movie_queue', false, false, false, false);

        $this->output->writeln('Waiting for messages to after adding new movie watcher. To exit press CTRL+C');

        $callback = function (AMQPMessage $msg) {
            $this->output->writeln("Received message: $msg->body from queue: after_add_movie_queue");

            $body = json_decode($msg->body, true, 512, JSON_THROW_ON_ERROR);

            $afterAddMovieRequest = $this->consumerAfterAddMovieRequestFactory->create($body['chatId'], $body['movieId']);

            $movie = $this->databaseMovieManager->getById($afterAddMovieRequest->getMovie());

            if ($movie === null) {
                throw AppException::movieWasNotFoundInDatabaseAfterAddMovie($afterAddMovieRequest->getMovie());
            }

            $message = <<<MESSAGE
            New movie added: $movie->title

            Thank you for your help!
            MESSAGE;

            $this->messageSender->send($afterAddMovieRequest->getChatId(), new SendMessageText($message));
        };

        $receiveChannel->basic_consume('after_add_movie_queue', '', false, true, false, false, $callback);

        $receiveChannel->consume();
    }
}
