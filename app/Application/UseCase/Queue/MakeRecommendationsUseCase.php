<?php

declare(strict_types=1);

namespace App\Application\UseCase\Queue;

use App\Application\Request\MakeRecommendationsRequest;
use App\Application\Telegram\Factory\ConsumerMakeRecommendationsRequestFactory;
use App\Application\Telegram\Factory\PublisherMakeRecommendationsResponseQueueMessageFactory;
use App\Application\UseCase\MakeRecommendationsUseCase as MakeRecommendationsAlgo;
use App\Domain\ValueObject\Queue\PublisherMakeRecommendationsResponse;
use Laravel\Prompts\Output\ConsoleOutput;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

readonly class MakeRecommendationsUseCase
{
    public function __construct(
        private AMQPStreamConnection                                    $connection,
        private ConsoleOutput                                           $output,
        private ConsumerMakeRecommendationsRequestFactory               $consumerMakeRecommendationsRequestFactory,
        private MakeRecommendationsAlgo                                 $makeRecommendationsAlgo,
        private PublisherMakeRecommendationsResponseQueueMessageFactory $messageFactory,
    )
    {
    }

    /**
     * @throws \ErrorException
     */
    public function execute(): void
    {
        $receiveChannel = $this->connection->channel();
        $sendChannel = $this->connection->channel();

        $receiveChannel->queue_declare('make_recommendations_queue', false, false, false, false);
        $sendChannel->queue_declare('after_make_recommendations_queue', false, false, false, false);

        $this->output->writeln('Waiting for messages to make recommendations. To exit press CTRL+C');

        $callback = function (AMQPMessage $msg) use ($sendChannel) {
            $this->output->writeln("Received message: $msg->body from queue: make_recommendations_queue");

            $body = json_decode($msg->body, true, 512, JSON_THROW_ON_ERROR);

            $makeRecommendationsRequest = $this->consumerMakeRecommendationsRequestFactory->create($body['chatId']);
            $dto = new MakeRecommendationsRequest($makeRecommendationsRequest->getChatId()->getChatId());
            $this->makeRecommendationsAlgo->execute($dto);

            $makeRecommendationsResponse = new PublisherMakeRecommendationsResponse($makeRecommendationsRequest->getChatId());
            $message = $this->messageFactory->create($makeRecommendationsResponse);

            $sendChannel->basic_publish($message, '', 'after_make_recommendations_queue');

            $this->output->writeln("Sent message: $message->body to queue: after_make_recommendations_queue");
        };

        $receiveChannel->basic_consume('make_recommendations_queue', '', false, true, false, false, $callback);

        $receiveChannel->consume();
    }
}
