<?php

declare(strict_types=1);

namespace App\Application\UseCase\Queue;

use App\Application\Request\AddNewMovieRequest;
use App\Application\Telegram\Factory\ConsumerAddMovieRequestFactory;
use App\Application\Telegram\Factory\PublisherAddMovieResponseFactory;
use App\Application\Telegram\Factory\PublisherResponseAddMovieQueueMessageFactory;
use App\Application\UseCase\AddNewMovieUseCase;
use Laravel\Prompts\Output\ConsoleOutput;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

readonly class AddMovieUseCase
{
    public function __construct(
        private AddNewMovieUseCase                           $useCase,
        private ConsumerAddMovieRequestFactory               $requestFactory,
        private AMQPStreamConnection                         $connection,
        private PublisherAddMovieResponseFactory             $responseFactory,
        private PublisherResponseAddMovieQueueMessageFactory $messageFactory,
        private ConsoleOutput                                $output,
    )
    {
    }

    /**
     * @throws \ErrorException
     */
    public function execute(): void
    {
        $receiveChannel = $this->connection->channel();
        $sendChannel = $this->connection->channel();

        $receiveChannel->queue_declare('add_movie_queue', false, false, false, false);
        $sendChannel->queue_declare('after_add_movie_queue', false, false, false, false);

        $this->output->writeln('Waiting for messages to add new movie. To exit press CTRL+C');

        $callback = function (AMQPMessage $msg) use ($sendChannel) {
            $this->output->writeln("Received message: $msg->body from queue: add_movie_queue");

            $body = json_decode($msg->body, true, 512, JSON_THROW_ON_ERROR);

            $addMovieRequest = $this->requestFactory->create($body['movieUrl']);

            $dto = new AddNewMovieRequest($addMovieRequest->getMovieUrl()->getUrl());
            $useCaseResponse = $this->useCase->execute($dto);

            $addMovieResponse = $this->responseFactory->create($useCaseResponse->movieId, $body['chatId']);
            $message = $this->messageFactory->create($addMovieResponse);

            $sendChannel->basic_publish($message, '', 'after_add_movie_queue');

            $this->output->writeln("Sent message: $message->body to queue: after_add_movie_queue");
        };

        $receiveChannel->basic_consume('add_movie_queue', '', false, true, false, false, $callback);

        $receiveChannel->consume();
    }
}
