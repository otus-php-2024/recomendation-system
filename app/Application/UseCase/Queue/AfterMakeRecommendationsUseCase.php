<?php

declare(strict_types=1);

namespace App\Application\UseCase\Queue;

use App\Application\Request\Telegram\GetListOfRecommendationsRequest;
use App\Application\Telegram\Factory\ConsumerAfterMakeRecommendationsRequestFactory;
use App\Application\Telegram\RecommendationCache;
use App\Application\UseCase\Telegram\GetListOfRecommendationsUseCase;
use ErrorException;
use Laravel\Prompts\Output\ConsoleOutput;
use LogicException;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

readonly class AfterMakeRecommendationsUseCase
{
    public function __construct(
        private AMQPStreamConnection                           $connection,
        private ConsumerAfterMakeRecommendationsRequestFactory $requestFactory,
        private ConsoleOutput                                  $output,
        private RecommendationCache                            $recommendationCache,
        private GetListOfRecommendationsUseCase                $getListOfRecommendationsUseCase,
    )
    {
    }

    /**
     * @throws ErrorException
     */
    public function execute(): void
    {
        $receiveChannel = $this->connection->channel();

        $receiveChannel->queue_declare('after_make_recommendations_queue', false, false, false, false);

        $this->output->writeln('Waiting for messages to after adding new movie watcher. To exit press CTRL+C');

        $callback = function (AMQPMessage $msg) {
            $this->output->writeln("Received message: $msg->body from queue: after_make_recommendations_queue");

            $body = json_decode($msg->body, true, 512, JSON_THROW_ON_ERROR);

            $makeRecommendationsRequest = $this->requestFactory->create($body['chatId']);

            if (!$this->recommendationCache->has($makeRecommendationsRequest->getChatId())) {
                throw new LogicException(
                    'Recommendations for chat id: ' . $makeRecommendationsRequest->getChatId()->getChatId() .
                    ' were not found in cache'
                );
            }

            $dto = new GetListOfRecommendationsRequest($makeRecommendationsRequest->getChatId()->getChatId());
            $this->getListOfRecommendationsUseCase->execute($dto);
        };

        $receiveChannel->basic_consume('after_make_recommendations_queue', '', false, true, false, false, $callback);

        $receiveChannel->consume();
    }
}
