<?php

declare(strict_types=1);

namespace App\Application;

class MainStorage
{
    public function getAppFullPath(string $path): string
    {
        return storage_path('app/' . $path);
    }
}
