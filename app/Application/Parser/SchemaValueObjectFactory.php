<?php

declare(strict_types=1);

namespace App\Application\Parser;

use App\Application\Exceptions\ParserException;
use App\Domain\ValueObject\Parser\Actor;
use App\Domain\ValueObject\Parser\Description;
use App\Domain\ValueObject\Parser\Director;
use App\Domain\ValueObject\Parser\MovieName;
use App\Domain\ValueObject\Parser\Genre;
use App\Domain\ValueObject\Parser\Keywords;
use App\Domain\ValueObject\Parser\Poster;
use App\Domain\ValueObject\Parser\PublishedDate;
use App\Domain\ValueObject\Parser\Rating;
use App\Domain\ValueObject\Parser\Schema;
use App\Domain\ValueObject\Parser\Trailer;
use DateTime;

class SchemaValueObjectFactory
{
    /**
     * @throws ParserException
     */
    public function create(array $schema): Schema
    {
        return new Schema(
            new MovieName($schema['name']),
            new Poster($schema['image']),
            new Description($schema['description']),
            new Rating($schema['aggregateRating']['ratingValue']),
            $this->getGenres($schema),
            new Keywords($schema['keywords']),
            new Trailer($schema['trailer']['embedUrl']),
            $this->getActors($schema),
            $this->getDirectors($schema),
            new PublishedDate(new DateTIme($schema['datePublished']))
        );
    }

    /**
     * @param array $schema
     * @return Genre[]
     * @throws ParserException
     */
    private function getGenres(array $schema): array
    {
        $genres = [];

        foreach ($schema['genre'] as $genre) {
            $genres[] = new Genre($genre);
        }

        return $genres;
    }

    /**
     * @param array $schema
     * @return Actor[]
     * @throws ParserException
     */
    private function getActors(array $schema): array
    {
        $actors = [];

        foreach ($schema['actor'] as $actor) {
            $actors[] = new Actor($actor['name'], $actor['url']);
        }

        return $actors;
    }

    /**
     * @param array $schema
     * @return Director[]
     * @throws ParserException
     */
    private function getDirectors(array $schema): array
    {
        $directors = [];

        foreach ($schema['director'] as $director) {
            $directors[] = new Director($director['name'], $director['url']);
        }

        return $directors;
    }
}
