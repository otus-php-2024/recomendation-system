<?php

declare(strict_types=1);

namespace App\Application\Parser;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Crawler
{
    public function __construct(
        private readonly Client $httpClient,
        private readonly string $userToken,
    ) {
    }

    /**
     * @throws GuzzleException
     */
    public function parse(string $url): string
    {
        $response = $this->httpClient->request('GET', '/', [
            'query' => [
                'token' => $this->userToken,
                'url' => $url,
            ],
        ]);

        return $response->getBody()->getContents();
    }
}
