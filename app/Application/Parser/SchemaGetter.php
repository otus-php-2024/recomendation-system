<?php

declare(strict_types=1);

namespace App\Application\Parser;

use App\Application\Exceptions\ParserException;
use App\Domain\ValueObject\Parser\Schema;
use JsonException;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Sequentially;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SchemaGetter
{
    private const array VALID_ROOT_KEYS = [
        'name',
        'image',
        'description',
        'aggregateRating',
        'genre',
        'keywords',
        'trailer',
        'actor',
        'director',
        'datePublished',
    ];

    public function __construct(
        private readonly SchemaValueObjectFactory $schemaValueObjectFactory,
        private readonly ValidatorInterface $validator
    )
    {
    }

    /**
     * @throws JsonException
     * @throws ParserException
     */
    public function get(string $html): Schema
    {
        $schemaString = $this->getSchemaString($html);
        $schemaArray = $this->getSchemaArray($schemaString);

        $this->validateSchema($schemaArray);

        return $this->schemaValueObjectFactory->create($schemaArray);
    }

    /**
     * @throws ParserException
     */
    private function getSchemaString(string $html): string
    {
        $startPosition = strpos($html, '<script type="application/ld+json">');

        if ($startPosition === false) {
            throw ParserException::schemaNotFound();
        }

        $substr = substr($html, $startPosition);
        $endPosition = strpos($substr, '</script>');

        if ($endPosition === false) {
            throw ParserException::schemaEndTagNotFound();
        }

        $substr = substr($substr, 0, $endPosition);

        return str_replace('<script type="application/ld+json">', '', $substr);
    }

    /**
     * @throws JsonException
     */
    private function getSchemaArray(string $schemaString): array
    {
        $data = json_decode($schemaString, true, 512, JSON_THROW_ON_ERROR);

        return array_filter(
            $data,
            static fn($key) => in_array($key, self::VALID_ROOT_KEYS, true),
            ARRAY_FILTER_USE_KEY);
    }

    /**
     * @throws ParserException
     */
    private function validateSchema(array $schema): void
    {
        $constrains = new Collection([
            'name' => new Sequentially([
                new NotBlank(),
            ]),
            'image' => new Sequentially([
                new NotBlank(),
                new Url(protocols: ['https']),
                new Regex(pattern: '/\.(jpg|jpeg|png|gif|webp)$/i'),
            ]),
            'description' => new Sequentially([
                new NotBlank(),
            ]),
            'aggregateRating' => new Sequentially([
                new NotBlank(),
                new Collection([
                    'ratingValue' => new Sequentially([
                        new NotBlank(),
                        new Range(min: 0, max: 10)
                    ]),
                    '@type' => new Optional(),
                    'ratingCount' => new Optional(),
                    'bestRating' => new Optional(),
                    'worstRating' => new Optional(),
                ]),
            ]),
            'genre' => new Sequentially([
                new NotBlank(),
                new Type('array'),
                new Count(min: 1),
                new All([
                    new Sequentially([
                        new NotBlank(),
                    ]),
                ])
            ]),
            'datePublished' => new Sequentially([
                new NotBlank(),
                new Date()
            ]),
            'keywords' => new Sequentially([
                new NotBlank(),
            ]),
            'trailer' => new Sequentially([
                new NotBlank(),
                new Collection([
                    'embedUrl' => new Sequentially([
                        new NotBlank(),
                        new Url(protocols: ['https']),
                    ]),
                    '@type' => new Optional(),
                    'name' => new Optional(),
                    'thumbnail' => new Optional(),
                    'thumbnailUrl' => new Optional(),
                    'url' => new Optional(),
                    'description' => new Optional(),
                    'duration' => new Optional(),
                    'uploadDate' => new Optional(),
                ]),
            ]),
            'actor' => new Sequentially([
                new NotBlank(),
                new Type('array'),
                new Count(min: 1),
                new All([
                    new Sequentially([
                        new NotBlank(),
                        new Collection([
                            'name' => new Sequentially([
                                new NotBlank(),
                            ]),
                            'url' => new Sequentially([
                                new NotBlank(),
                                new Url(protocols: ['https']),
                            ]),
                            '@type' => new Optional(),
                        ]),
                    ]),
                ]),
            ]),
            'director' => new Sequentially([
                new NotBlank(),
                new Type('array'),
                new Count(min: 1),
                new All([
                    new Sequentially([
                        new NotBlank(),
                        new Collection([
                            'name' => new Sequentially([
                                new NotBlank(),
                            ]),
                            'url' => new Sequentially([
                                new NotBlank(),
                                new Url(protocols: ['https']),
                            ]),
                            '@type' => new Optional(),
                        ]),
                    ]),
                ]),
            ]),
        ]);

        $violations = $this->validator->validate($schema, $constrains);

        if ($violations->count() > 0) {
            throw ParserException::validationError(array_map(
                static fn($violation) => [
                    'property' => $violation->getPropertyPath(),
                    'message' => $violation->getMessage()
                ],
                iterator_to_array($violations)
            ));
        }
    }
}
