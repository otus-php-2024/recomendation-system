<?php

declare(strict_types=1);

namespace App\Application\AddNewMovie;

class PageStorage extends BaseStore
{
    protected function getPathToStorage(): string
    {
        return 'pages/';
    }
}
