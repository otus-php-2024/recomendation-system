<?php

declare(strict_types=1);

namespace App\Application\AddNewMovie;

use App\AppException;

class PosterDownloader
{
    /**
     * @throws AppException
     */
    public function download(string $url): string
    {
        $content = file_get_contents($url);

        if ($content === false) {
            throw AppException::cannotDownloadPoster($url);
        }

        return $content;
    }
}
