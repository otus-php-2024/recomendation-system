<?php

declare(strict_types=1);

namespace App\Application\AddNewMovie;

use Illuminate\Support\Facades\Storage;

abstract class BaseStore
{
    abstract protected function getPathToStorage(): string;

    public function store(string $filename, string $content): void
    {
        Storage::put($this->getPathToFile($filename), $content);
    }

    public function getPathToFile(string $filename): string
    {
        return $this->getPathToStorage() . $filename;
    }

    public function exists(string $filename): bool
    {
        return Storage::exists($this->getPathToFile($filename));
    }

    public function get(string $filename): string
    {
        return Storage::get($this->getPathToFile($filename));
    }
}
