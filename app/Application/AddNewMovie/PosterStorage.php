<?php

declare(strict_types=1);

namespace App\Application\AddNewMovie;

class PosterStorage extends BaseStore
{
    protected function getPathToStorage(): string
    {
        return 'posters/';
    }
}
