<?php

declare(strict_types=1);

namespace App\Application\AddNewMovie;

use App\Domain\Models\Actor;
use App\Domain\Models\Director;
use App\Domain\Models\Genre;
use App\Domain\Models\Movie;
use App\Domain\ValueObject\AddNewMovie\MovieUrl;
use App\Domain\ValueObject\AddNewMovie\PosterStoragePath;
use App\Domain\ValueObject\Parser\Actor as ActorValueBoject;
use App\Domain\ValueObject\Parser\Director as DirectorValueObject;
use App\Domain\ValueObject\Parser\Genre as GenreValueObject;
use App\Domain\ValueObject\Parser\Schema;
use Illuminate\Support\Facades\DB;
use Throwable;

class DatabaseMovieCreator
{
    /**
     * @throws Throwable
     */
    public function create(MovieUrl $movieUrl, Schema $schema, PosterStoragePath $posterStoragePath): Movie
    {
        DB::beginTransaction();

        try {
            $movie = $this->updateMovie($movieUrl, $schema, $posterStoragePath);

            $movie->save();

            $this->updateMovieGenres($movie, $this->getGenres($schema->getGenres()));
            $this->updateMovieActors($movie, $this->getActors($schema->getActors()));
            $this->updateMovieDirectors($movie, $this->getDirectors($schema->getDirectors()));

            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();

            throw $e;
        }

        return $movie;
    }

    private function updateMovie(MovieUrl $movieUrl, Schema $schema, PosterStoragePath $posterStoragePath): Movie
    {
        $movie = Movie::firstOrNew(['source_url' => $movieUrl->getUrl()]);

        $movie->title = $schema->getMovieName()->getName();
        $movie->poster_path = $posterStoragePath->getPath();
        $movie->description = $schema->getDescription()->getDescription();
        $movie->imdb_rating = $schema->getRating()->getRating();
        $movie->keywords = $schema->getKeywords()->getKeywords();
        $movie->trailer_url = $schema->getTrailer()->getTrailer();
        $movie->published_date = $schema->getPublishedDate()->getPublishedDate();

        return $movie;
    }

    /**
     * @param GenreValueObject[] $genres
     * @return Genre[]
     */
    private function getGenres(array $genres): array
    {
        $genreModels = [];

        foreach ($genres as $genre) {
            $genreModels[] = Genre::firstOrCreate(['genre' => $genre->getGenre()]);
        }

        return $genreModels;
    }

    /**
     * @param Genre[] $genres
     */
    private function updateMovieGenres(Movie $movie, array $genres): void
    {
        $skipAttached = [];
        /** @var Genre[] $attached */
        $attachedGenres = $movie->genres;

        foreach ($genres as $genre) {
            foreach ($attachedGenres as $attachedGenre) {
                if ($genre->id === $attachedGenre->id) {
                    $skipAttached[] = $genre->getId();
                    continue 2;
                }
            }

            $movie->genres()->attach($genre->id);
        }

        foreach ($attachedGenres as $attachedGenre) {
            if (!in_array($attachedGenre->id, $skipAttached, true)) {
                $movie->genres()->detach($attachedGenre->id);
            }
        }
    }

    /**
     * @param ActorValueBoject[] $actors
     * @return Actor[]
     */
    private function getActors(array $actors): array
    {
        $actorModels = [];

        foreach ($actors as $actor) {
            $actorModels[] = Actor::firstOrCreate(['name' => $actor->getName(), 'url' => $actor->getUrl()]);
        }

        return $actorModels;
    }

    /**
     * @param Actor[] $actors
     */
    private function updateMovieActors(Movie $movie, array $actors): void
    {
        $skipAttached = [];
        /** @var Actor[] $attached */
        $attachedActors = $movie->actors;

        foreach ($actors as $actor) {
            foreach ($attachedActors as $attachedActor) {
                if ($actor->id === $attachedActor->id) {
                    $skipAttached[] = $actor->getId();
                    continue 2;
                }
            }

            $movie->actors()->attach($actor->id);
        }

        foreach ($attachedActors as $attachedActor) {
            if (!in_array($attachedActor->id, $skipAttached, true)) {
                $movie->actors()->detach($attachedActor->id);
            }
        }
    }

    /**
     * @param DirectorValueObject[] $directors
     * @return Director[]
     */
    private function getDirectors(array $directors): array
    {
        $directorModels = [];

        foreach ($directors as $director) {
            $directorModels[] = Director::firstOrCreate(['name' => $director->getName(), 'url' => $director->getUrl()]);
        }

        return $directorModels;
    }

    /**
     * @param Director[] $directors
     */
    private function updateMovieDirectors(Movie $movie, array $directors): void
    {
        $skipAttached = [];
        /** @var Director[] $attached */
        $attachedDirectors = $movie->directors;

        foreach ($directors as $director) {
            foreach ($attachedDirectors as $attachedDirector) {
                if ($director->id === $attachedDirector->id) {
                    $skipAttached[] = $director->getId();
                    continue 2;
                }
            }

            $movie->directors()->attach($director->id);
        }

        foreach ($attachedDirectors as $attachedDirector) {
            if (!in_array($attachedDirector->id, $skipAttached, true)) {
                $movie->directors()->detach($attachedDirector->id);
            }
        }
    }
}
