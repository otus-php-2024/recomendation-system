<?php

declare(strict_types=1);

namespace App\Application\Request\Telegram;

readonly class AddMovieCommandRequest
{
    public function __construct(
        public int $chatId
    ) {
    }
}
