<?php

declare(strict_types=1);

namespace App\Application\Request\Telegram;

readonly class ConversationRequest
{
    public function __construct(
        public int     $chatId,
        public int     $dateTimestamp,
        public string  $message,
        public ?string $botCommand = null,
        public ?string $callbackData = null,
    )
    {
    }
}
