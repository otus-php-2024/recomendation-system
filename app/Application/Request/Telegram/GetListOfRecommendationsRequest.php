<?php

declare(strict_types=1);

namespace App\Application\Request\Telegram;

readonly class GetListOfRecommendationsRequest
{
    public function __construct(
        public int $chatId,
        public int $offset = 0
    ) {
    }
}
