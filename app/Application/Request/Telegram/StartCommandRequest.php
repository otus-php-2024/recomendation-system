<?php

declare(strict_types=1);

namespace App\Application\Request\Telegram;

readonly class StartCommandRequest
{
    public function __construct(
        public int $userId,
        public int $chatId,
    )
    {
    }
}
