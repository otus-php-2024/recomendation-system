<?php

declare(strict_types=1);

namespace App\Application\Request\Telegram;

readonly class RateMovieRequest
{
    public function __construct(
        public int $chatId,
        public ?int $movieId,
        public ?int $rating
    ) {
    }
}
