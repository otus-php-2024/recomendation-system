<?php

declare(strict_types=1);

namespace App\Application\Request;

readonly class AddNewMovieRequest
{
    public function __construct(
        public ?string $url = null,
    ) {
    }
}
