<?php

declare(strict_types=1);

namespace App\Application\Request;

readonly class MakeRecommendationsRequest
{
    public function __construct(public int $chatId)
    {
    }
}
