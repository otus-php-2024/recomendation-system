<?php

declare(strict_types=1);

namespace App\Application\RecommendationSystem\Vector;

use App\AppException;
use App\Domain\ValueObject\RecommendationSystem\Vector\SVD;
use App\Domain\ValueObject\RecommendationSystem\Vector\UsersMatrix;
use MathPHP\Exception\MathException;
use MathPHP\LinearAlgebra\NumericMatrix;

class SVDMatrixGetter
{
    /**
     * @throws MathException|AppException
     */
    public function getSVD(UsersMatrix $matrixArray): SVD
    {
        $matrix = $matrixArray->getMatrix();

        if (! $matrix instanceof NumericMatrix) {
            throw AppException::matrixMustBeNumericMatrix($matrix);
        }

        $result = $matrix->svd();

        return new SVD(
            $result->getU(),
            $result->getS(),
            $result->getV()
        );
    }
}
