<?php

declare(strict_types=1);

namespace App\Application\RecommendationSystem\Vector;

use App\AppException;
use App\Domain\UserRatingsIteratorInterface;
use App\Domain\ValueObject\RecommendationSystem\Vector\UsersMatrix;
use MathPHP\Exception\BadDataException;
use MathPHP\Exception\IncorrectTypeException;
use MathPHP\Exception\MathException;
use MathPHP\Exception\MatrixException;
use MathPHP\LinearAlgebra\Matrix;
use MathPHP\LinearAlgebra\MatrixFactory;

class UsersMatrixCreator
{
    /**
     * @param UserRatingsIteratorInterface $usersRatings
     * @return UsersMatrix
     * @throws AppException
     * @throws BadDataException
     * @throws IncorrectTypeException
     * @throws MathException
     * @throws MatrixException
     */
    public function create(UserRatingsIteratorInterface $usersRatings): UsersMatrix
    {
        $matrixLikeData = $this->getMatrixLikeData($usersRatings);

        if (empty($matrixLikeData)) {
            throw AppException::matrixLikeDataCannotBeEmpty();
        }

        $users = array_keys($matrixLikeData);
        $movies = array_keys($matrixLikeData[$users[0]]);
        $matrix = $this->createMatrix($matrixLikeData);

        return new UsersMatrix($matrix, $users, $movies);
    }

    private function getMatrixLikeData(UserRatingsIteratorInterface $iterator): array
    {
        $matrix = [];
        $attachedMovies = [];

        foreach ($iterator as $userRatingsValueObject) {
            $matrix[$userRatingsValueObject->getUser()->getUserId()] = [];

            foreach ($userRatingsValueObject->getRatings() as $rating) {
                $matrix[$userRatingsValueObject->getUser()->getUserId()][$rating->getMovie()->getMovieId()] = $rating->getRating()->getRatingValue();
                $attachedMovies[$rating->getMovie()->getMovieId()] = true;
            }
        }

        $movies = array_keys($attachedMovies);

        foreach ($matrix as &$ratings) {
            foreach ($movies as $movie) {
                if (!isset($ratings[$movie])) {
                    $ratings[$movie] = 0;
                }
            }

            ksort($ratings, SORT_NUMERIC);
        }

        return $matrix;
    }

    /**
     * @throws IncorrectTypeException
     * @throws MatrixException
     * @throws BadDataException
     * @throws MathException
     */
    private function createMatrix(array $matrixLikeData): Matrix
    {
        $matrix = [];

        foreach ($matrixLikeData as $row) {
            $matrix[] = array_values($row);
        }

        return MatrixFactory::create($matrix);
    }
}
