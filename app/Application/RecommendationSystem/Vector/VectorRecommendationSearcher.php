<?php

declare(strict_types=1);

namespace App\Application\RecommendationSystem\Vector;

use App\AppException;
use App\Application\RecommendationSystem\ConvertRecommendationsToResultTrait;
use App\Application\RecommendationSystem\RecommendationFactory;
use App\Domain\RecommendationSearchInterface;
use App\Domain\UserRatingsIteratorInterface;
use App\Domain\ValueObject\RecommendationSystem\Recommendation;
use App\Domain\ValueObject\RecommendationSystem\TargetUser;
use App\Domain\ValueObject\RecommendationSystem\Vector\SVD;
use App\Domain\ValueObject\RecommendationSystem\Vector\UsersMatrix;
use MathPHP\Exception\MathException;
use MathPHP\Exception\MatrixException;

class VectorRecommendationSearcher implements RecommendationSearchInterface
{
    use ConvertRecommendationsToResultTrait;

    public function __construct(
        private readonly RecommendationFactory $recommendationFactory,
        private readonly UsersMatrixCreator $usersMatrixCreator,
        private readonly SVDMatrixGetter $svdMatrixGetter,
    )
    {
    }

    /**
     * @throws MatrixException
     * @throws MathException
     * @throws AppException
     * @return Recommendation[]
     */
    public function search(UserRatingsIteratorInterface $iterator, TargetUser $targetUser): array
    {
        $matrix = $this->usersMatrixCreator->create($iterator);
        $svd = $this->svdMatrixGetter->getSVD($matrix);
        $recommendations = $this->getSVDRecommendations($targetUser, $svd, $matrix);

        return $this->convertRecommendationsToValueObjects($recommendations);
    }


    /**
     * Function to get movie recommendations using SVD
     *
     * @throws MatrixException
     * @throws AppException
     */
    private function getSVDRecommendations(TargetUser $targetUser, SVD $svd, UsersMatrix $matrix): array
    {
        $targetUserPosition = $matrix->getUserPosition($targetUser->getUserId());

        if ($targetUserPosition === null) {
            throw AppException::targetUserNotFoundInMatrix($targetUser->getUserId());
        }

        $rawMatrix = $matrix->getMatrix()->getMatrix();

        // Extract user and item latent factors
        $userLatentFactor = $svd->getU()->getRow($targetUserPosition);

        $recommendations = [];

        foreach ($svd->getV()->getMatrix() as $moviePosition => $itemLatentFactor) {
            // Calculate the predicted rating using the dot product of user and item latent factors
            $predictedRating = array_sum(array_map(static fn($u, $v) => $u * $v, $userLatentFactor, $itemLatentFactor));

            $movieId = $matrix->getMovieId($moviePosition);

            if ($movieId === null) {
                throw AppException::movieIdNotFoundInMatrix($moviePosition);
            }

            // Consider movies the user hasn't rated yet
            if ($rawMatrix[$targetUserPosition][$moviePosition] === 0) {
                $recommendations[$movieId] = abs($predictedRating);
            }
        }

        arsort($recommendations, SORT_NUMERIC); // Sort recommendations by predicted rating in descending order

        return $recommendations;
    }
}
