<?php

declare(strict_types=1);

namespace App\Application\RecommendationSystem;

use App\AppException;

class UsersRatingsIteratorFactory
{
    /**
     * @throws AppException
     */
    public function create(): UsersRatingsIterator
    {
        return new UsersRatingsIterator(new UsersRatingsGenerator());
    }
}
