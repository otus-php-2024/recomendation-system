<?php

namespace App\Application\RecommendationSystem;

use App\AppException;
use App\Domain\ValueObject\RecommendationSystem\Movie;
use App\Domain\ValueObject\RecommendationSystem\Recommendation;
use App\Domain\ValueObject\RecommendationSystem\Score;

trait ConvertRecommendationsToResultTrait
{
    /**
     * @return Recommendation[]
     * @throws AppException
     */
    private function convertRecommendationsToValueObjects(array $recommendations): array
    {
        $recommendationsValueObjects = [];

        foreach ($recommendations as $movie => $score) {
            $recommendationsValueObjects[] = new Recommendation(new Movie($movie), new Score($score));
        }

        return $recommendationsValueObjects;
    }
}
