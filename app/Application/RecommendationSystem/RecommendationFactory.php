<?php

declare(strict_types=1);

namespace App\Application\RecommendationSystem;

use App\AppException;
use App\Domain\ValueObject\RecommendationSystem\Movie;
use App\Domain\ValueObject\RecommendationSystem\Recommendation;
use App\Domain\ValueObject\RecommendationSystem\Score;

class RecommendationFactory
{
    /**
     * @throws AppException
     */
    public function create(int $movieId, int $scoreId): Recommendation
    {
        return new Recommendation(new Movie($movieId), new Score($scoreId));
    }
}
