<?php

declare(strict_types=1);

namespace App\Application\RecommendationSystem;

use App\AppException;
use App\Domain\UserRatingsIteratorInterface;
use App\Domain\ValueObject\RecommendationSystem\Recommendation;
use App\Domain\ValueObject\RecommendationSystem\TargetUser;
use App\Domain\RecommendationSearchInterface;

class RecommendationSearcher implements RecommendationSearchInterface
{
    use ConvertRecommendationsToResultTrait;

    public function __construct(
        private readonly RecommendationFactory $recommendationFactory,
    )
    {
    }

    /**
     * @param UserRatingsIteratorInterface $iterator
     * @param TargetUser $targetUser
     * @return Recommendation[]
     * @throws AppException
     */
    public function search(UserRatingsIteratorInterface $iterator, TargetUser $targetUser): array
    {
        $userRatings = $this->getUsersRatings($iterator);
        $recommendations = $this->getMovieRecommendations($targetUser, $userRatings);

        return $this->convertRecommendationsToValueObjects($recommendations);
    }

    private function getUsersRatings(UserRatingsIteratorInterface $iterator): array
    {
        $usersRatings = [];

        foreach ($iterator as $userRatingsValueObject) {
            $usersRatings[$userRatingsValueObject->getUser()->getUserId()] = [];

            foreach ($userRatingsValueObject->getRatings() as $rating) {
                $usersRatings[$userRatingsValueObject->getUser()->getUserId()][$rating->getMovie()->getMovieId()] = $rating->getRating()->getRatingValue();
            }
        }

        return $usersRatings;
    }

    private function getMovieRecommendations(TargetUser $targetUser, array $userRatings): array
    {
        if (!array_key_exists($targetUser->getUserId(), $userRatings)) {
            return [];
        }

        $similarities = [];

        foreach ($userRatings as $user => $ratings) {
            if ($user !== $targetUser->getUserId()) {
                $similarity = $this->calculateSimilarity($userRatings[$targetUser->getUserId()], $ratings);
                $similarities[$user] = $similarity;
            }
        }

        arsort($similarities); // Sort similarities in descending order

        $recommendations = [];

        foreach ($similarities as $user => $similarity) {
            foreach ($userRatings[$user] as $movie => $rating) {
                if (!isset($userRatings[$targetUser->getUserId()][$movie]) || $userRatings[$targetUser->getUserId()][$movie] === 0) {
                    $recommendations[$movie] = $recommendations[$movie] ?? 0;
                    $recommendations[$movie] += $rating * $similarity;
                }
            }
        }

        arsort($recommendations); // Sort recommendations by score in descending order

        return $recommendations;
    }


    // Function to calculate similarity between two users using Euclidean distance
    private function calculateSimilarity(array $userRatings, array $otherUserRatings): float|int
    {
        $commonMovies = array_intersect_key($userRatings, $otherUserRatings);

        if (empty($commonMovies)) {
            return 0; // No common movies, similarity is zero
        }

        $squaredDifferences = [];

        foreach ($commonMovies as $movie => $rating) {
            $squaredDifferences[] = ($userRatings[$movie] - $otherUserRatings[$movie]) ** 2;
        }

        $sumSquaredDifferences = array_sum($squaredDifferences);

        return 1 / (1 + sqrt($sumSquaredDifferences));
    }
}
