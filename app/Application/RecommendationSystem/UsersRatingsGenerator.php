<?php

declare(strict_types=1);

namespace App\Application\RecommendationSystem;

use App\AppException;
use App\Domain\Models\Rating;
use App\Domain\ValueObject\RecommendationSystem\Movie;
use App\Domain\ValueObject\RecommendationSystem\MovieRating;
use App\Domain\ValueObject\RecommendationSystem\Rating as RatingVO;
use App\Domain\ValueObject\RecommendationSystem\User;
use App\Domain\ValueObject\RecommendationSystem\UserRatings;
use Generator;
use Illuminate\Database\Eloquent\Collection;

class UsersRatingsGenerator
{
    /**
     * @throws AppException
     */
    public function getUsersRatings(): Generator
    {
        $startUser = $this->getNextUser(0);

        while ($startUser !== null) {
            echo "Processing user $startUser\n";
            /** @var Collection<Rating> $userRatings */
            $userRatings = Rating::where('user_id', $startUser)->orderBy('movie_id')->get();

            if ($userRatings->isEmpty()) {
                $startUser = $this->getNextUser($startUser);
                continue;
            }

            $userValueObject = new User($startUser);
            $ratingSValueObjects = [];

            foreach ($userRatings as $rating) {
                $ratingSValueObjects[] = new MovieRating(
                    new Movie($rating->movie_id),
                    new RatingVO($rating->rating)
                );
            }

            $startUser = $this->getNextUser($startUser);

            yield new UserRatings($userValueObject, $ratingSValueObjects);
        }
    }

    private function getNextUser(int $userId): ?int
    {
        /** @var Rating|null $result */
        $result = Rating::where('user_id', '>', $userId)->orderBy('user_id')->first();

        return $result?->user_id;
    }
}
