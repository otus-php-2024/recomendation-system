<?php

declare(strict_types=1);

namespace App\Application\RecommendationSystem;

use App\AppException;
use App\Domain\UserRatingsIteratorInterface;
use App\Domain\ValueObject\RecommendationSystem\UserRatings;
use Generator;

class UsersRatingsIterator implements UserRatingsIteratorInterface
{
    private Generator $usersRatingsGenerator;

    /**
     * @throws AppException
     */
    public function __construct(UsersRatingsGenerator $usersRatingsGenerator)
    {
        $this->usersRatingsGenerator = $usersRatingsGenerator->getUsersRatings();
    }

    public function next(): void
    {
        $this->usersRatingsGenerator->next();
    }

    public function valid(): bool
    {
        return $this->usersRatingsGenerator->valid();
    }

    public function rewind(): void
    {
        $this->usersRatingsGenerator->rewind();
    }

    /**
     * @throws AppException
     */
    public function current(): UserRatings
    {
        $current = $this->usersRatingsGenerator->current();

        if (! $current instanceof UserRatings) {
            throw AppException::invalidUserRatingsValue($current);
        }

        return $current;
    }

    public function key(): int
    {
        return $this->usersRatingsGenerator->key();
    }
}
