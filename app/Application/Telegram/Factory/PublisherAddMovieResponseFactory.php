<?php

declare(strict_types=1);

namespace App\Application\Telegram\Factory;

use App\AppException;
use App\Application\Exceptions\TelegramException;
use App\Domain\ValueObject\Queue\PublisherAddMovieResponse;
use App\Domain\ValueObject\RecommendationSystem\Movie;
use App\Domain\ValueObject\Telegram\ChatId;

class PublisherAddMovieResponseFactory
{
    /**
     * @throws AppException
     * @throws TelegramException
     */
    public function create(int $movieId, int $chatId): PublisherAddMovieResponse
    {
        return new PublisherAddMovieResponse(new Movie($movieId), new ChatId($chatId));
    }
}
