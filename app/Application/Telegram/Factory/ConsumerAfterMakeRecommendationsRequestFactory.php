<?php

declare(strict_types=1);

namespace App\Application\Telegram\Factory;

use App\Application\Exceptions\TelegramException;
use App\Domain\ValueObject\Queue\ConsumerAfterMakeRecommendationsRequest;
use App\Domain\ValueObject\Telegram\ChatId;

class ConsumerAfterMakeRecommendationsRequestFactory
{
    /**
     * @throws TelegramException
     */
    public function create(int $chatId): ConsumerAfterMakeRecommendationsRequest
    {
        return new ConsumerAfterMakeRecommendationsRequest(new ChatId($chatId));
    }
}
