<?php

declare(strict_types=1);

namespace App\Application\Telegram\Factory;

use App\Domain\ValueObject\Queue\PublisherMakeRecommendationsResponse;
use PhpAmqpLib\Message\AMQPMessage;

class PublisherMakeRecommendationsResponseQueueMessageFactory
{
    /**
     * @throws \JsonException
     */
    public function create(PublisherMakeRecommendationsResponse $response): AMQPMessage
    {
        return new AMQPMessage(
            json_encode([
                'chatId' => $response->getChatId()->getChatId(),
            ], JSON_THROW_ON_ERROR)
        );
    }
}
