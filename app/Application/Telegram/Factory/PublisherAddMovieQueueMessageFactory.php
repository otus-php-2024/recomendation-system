<?php

declare(strict_types=1);

namespace App\Application\Telegram\Factory;

use App\Domain\ValueObject\Queue\PublisherAfterAddMovieRequest;
use JsonException;
use PhpAmqpLib\Message\AMQPMessage;

class PublisherAddMovieQueueMessageFactory
{
    /**
     * @throws JsonException
     */
    public function create(PublisherAfterAddMovieRequest $request): AMQPMessage
    {
        $message = json_encode(
            [
                'chatId' => $request->getChatId()->getChatId(),
                'movieUrl' => $request->getMovieUrl()->getUrl(),
            ],
            JSON_THROW_ON_ERROR
        );

        return new AMQPMessage($message);
    }
}
