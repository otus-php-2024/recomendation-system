<?php

declare(strict_types=1);

namespace App\Application\Telegram\Factory;

use App\Domain\ValueObject\Queue\PublisherMakeRecommendationsRequest;
use PhpAmqpLib\Message\AMQPMessage;

class PublisherMakeRecommendationsQueueMessageFactory
{
    /**
     * @throws \JsonException
     */
    public function create(PublisherMakeRecommendationsRequest $request): AMQPMessage
    {
        return new AMQPMessage(
            json_encode([
                'chatId' => $request->getChatId()->getChatId(),
            ], JSON_THROW_ON_ERROR)
        );
    }
}
