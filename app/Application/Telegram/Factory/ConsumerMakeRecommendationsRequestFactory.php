<?php

declare(strict_types=1);

namespace App\Application\Telegram\Factory;

use App\Application\Exceptions\TelegramException;
use App\Domain\ValueObject\Queue\ConsumerMakeRecommendationsRequest;
use App\Domain\ValueObject\Telegram\ChatId;

class ConsumerMakeRecommendationsRequestFactory
{
    /**
     * @throws TelegramException
     */
    public function create(int $chatId): ConsumerMakeRecommendationsRequest
    {
        return new ConsumerMakeRecommendationsRequest(new ChatId($chatId));
    }
}
