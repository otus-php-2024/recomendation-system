<?php

declare(strict_types=1);

namespace App\Application\Telegram\Factory;

use App\AppException;
use App\Application\Exceptions\TelegramException;
use App\Domain\ValueObject\AddNewMovie\MovieUrl;
use App\Domain\ValueObject\Queue\PublisherAfterAddMovieRequest;
use App\Domain\ValueObject\Telegram\ChatId;

class PublisherAfterAddMovieRequestFactory
{
    /**
     * @throws AppException
     * @throws TelegramException
     */
    public function create(int $chatId, string $moveUrl): PublisherAfterAddMovieRequest
    {
        return new PublisherAfterAddMovieRequest(new ChatId($chatId), new MovieUrl($moveUrl));
    }
}
