<?php

declare(strict_types=1);

namespace App\Application\Telegram\Factory;

use App\AppException;
use App\Domain\ValueObject\AddNewMovie\MovieUrl;
use App\Domain\ValueObject\Queue\ConsumerAddMovieRequest;

class ConsumerAddMovieRequestFactory
{
    /**
     * @throws AppException
     */
    public function create(string $url): ConsumerAddMovieRequest
    {
        return new ConsumerAddMovieRequest(new MovieUrl($url));
    }
}
