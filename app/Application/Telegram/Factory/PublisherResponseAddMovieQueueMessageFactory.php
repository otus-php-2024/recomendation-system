<?php

declare(strict_types=1);

namespace App\Application\Telegram\Factory;

use App\Domain\ValueObject\Queue\PublisherAddMovieResponse;
use PhpAmqpLib\Message\AMQPMessage;

class PublisherResponseAddMovieQueueMessageFactory
{
    /**
     * @throws \JsonException
     */
    public function create(PublisherAddMovieResponse $response): AMQPMessage
    {
        $message = json_encode(
            [
                'chatId' => $response->getChatId()->getChatId(),
                'movieId' => $response->getMovie()->getMovieId(),
            ],
            JSON_THROW_ON_ERROR
        );

        return new AMQPMessage($message);
    }
}
