<?php

declare(strict_types=1);

namespace App\Application\Telegram\Factory;

use App\AppException;
use App\Application\Exceptions\TelegramException;
use App\Domain\ValueObject\Queue\ConsumerAfterAddMovieRequest;
use App\Domain\ValueObject\RecommendationSystem\Movie;
use App\Domain\ValueObject\Telegram\ChatId;

class ConsumerAfterAddMovieRequestFactory
{
    /**
     * @throws AppException
     * @throws TelegramException
     */
    public function create(int $chatId, int $movieId): ConsumerAfterAddMovieRequest
    {
        return new ConsumerAfterAddMovieRequest(new ChatId($chatId), new Movie($movieId));
    }
}
