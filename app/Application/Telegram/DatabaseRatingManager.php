<?php

declare(strict_types=1);

namespace App\Application\Telegram;

use App\Domain\Models\Rating;
use App\Domain\ValueObject\RecommendationSystem\Movie;
use App\Domain\ValueObject\RecommendationSystem\Rating as RatingVO;
use App\Domain\ValueObject\RecommendationSystem\User;

class DatabaseRatingManager
{
    public function rate(User $user, Movie $movie, RatingVO $rating): void
    {
        $databaseRating = Rating::where('user_id', $user->getUserId())
            ->where('movie_id', $movie->getMovieId())
            ->first();

        if ($databaseRating === null) {
            $databaseRating = new Rating();
            $databaseRating->user_id = $user->getUserId();
            $databaseRating->movie_id = $movie->getMovieId();
            $databaseRating->rating = $rating->getRatingValue();

            $databaseRating->save();
            return;
        }

        $databaseRating->rating = $rating->getRatingValue();
        $databaseRating->save();
    }
}
