<?php

declare(strict_types=1);

namespace App\Application\Telegram;

use App\Application\Exceptions\TelegramException;
use App\Domain\Models\Chat;
use App\Domain\Models\ChatStatus as ChatStatusModel;
use App\Domain\ValueObject\Telegram\ChatId;
use App\Domain\ValueObject\Telegram\ChatStatus;

class DatabaseChatStatusManager
{
    /**
     * @throws TelegramException
     */
    public function set(ChatId $chatId, ChatStatus $status): void
    {
        /** @var Chat|null $chat */
        $chat = Chat::where('telegram_chat_id', $chatId->getChatId())->first();

        if ($chat === null) {
            throw TelegramException::cantFindChat($chatId->getChatId());
        }

        /** @var ChatStatusModel|null $databaseStatus */
        $databaseStatus = $chat->chatStatus;

        if ($databaseStatus !== null) {
            $databaseStatus->status = $status->getStatus();

            $databaseStatus->save();

            return;
        }

        $chat->chatStatus()->create([
            'status' => $status->getStatus()
        ]);
    }

    public function get(ChatId $chatId): ?ChatStatusModel
    {
        /** @var Chat|null $chat */
        $chat = Chat::where('telegram_chat_id', $chatId->getChatId())->first();

        return $chat->chatStatus;
    }
}
