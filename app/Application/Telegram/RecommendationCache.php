<?php

declare(strict_types=1);

namespace App\Application\Telegram;

use App\Domain\ValueObject\RecommendationSystem\Movie;
use App\Domain\ValueObject\RecommendationSystem\Recommendation;
use App\Domain\ValueObject\Telegram\ChatId;
use Illuminate\Support\Facades\Redis;

class RecommendationCache
{
    public function has(ChatId $chatId): bool
    {
        return Redis::command('exists', [$this->getRecommendationCacheKey($chatId)]) === 1;
    }

    private function getRecommendationCacheKey(ChatId $chatId): string
    {
        return 'recommendation_' . $chatId->getChatId();
    }

    /**
     * @param ChatId $chatId
     * @return Movie[]
     * @throws \App\AppException
     */
    public function get(ChatId $chatId): array
    {
        $cacheResult = Redis::command('hgetall', [$this->getRecommendationCacheKey($chatId)]);
        $result = [];

        foreach ($cacheResult as $value) {
            $result[] = new Movie((int) $value);
        }

        return $result;
    }

    /**
     * @param ChatId $chatId
     * @param Recommendation[] $recommendation
     * @param int|null $expire
     * @return void
     */
    public function set(ChatId $chatId, array $recommendation, ?int $expire): void
    {
        $key = $this->getRecommendationCacheKey($chatId);

        if ($this->has($chatId)) {
            Redis::command('del', [$key]);
        }

        Redis::command('hmset', [$key, ...$this->convertRecommendationToString($recommendation)]);

        if ($expire !== null) {
            Redis::command('expire', [$key, $expire]);
        }
    }

    /**
     * @param Recommendation[] $recommendation
     * @return array
     */
    private function convertRecommendationToString(array $recommendation): array
    {
        $result = [];

        foreach ($recommendation as $index => $movie) {
            $result[] = $index;
            $result[] = $movie->getMovie()->getMovieId();
        }

        print_r($result);
        return $result;
    }
}
