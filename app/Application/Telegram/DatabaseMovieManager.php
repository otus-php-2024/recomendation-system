<?php

declare(strict_types=1);

namespace App\Application\Telegram;

use App\Domain\Models\Movie;
use App\Domain\ValueObject\AddNewMovie\MovieUrl;
use App\Domain\ValueObject\RecommendationSystem\Movie as MovieVO;
use Illuminate\Database\Eloquent\Collection;

class DatabaseMovieManager
{
    public function existsBySourceUrl(MovieUrl $movieUrl): bool
    {
        return Movie::where('source_url', $movieUrl->getUrl())->exists();
    }

    public function getBySourceUrl(MovieUrl $movieUrl): ?Movie
    {
        return Movie::where('source_url', $movieUrl->getUrl())->first();
    }

    public function getById(MovieVO $movie): ?Movie
    {
        return Movie::where('id', $movie->getMovieId())->first();
    }

    /**
     * @param int[] $movieIds
     * @return Movie[]
     */
    public function getByIds(array $movieIds): array
    {
        $queryResult = Movie::whereIn('id', $movieIds)->get();
        $result = [];

        foreach ($queryResult as $movie) {
            $result[$movie->id] = $movie;
        }

        return $result;
    }

    /**
     * @param int[] $movieIds
     * @return Collection<Movie>
     */
    public function getNotInIds(array $movieIds): Collection
    {
        return Movie::whereNotIn('id', $movieIds)->get();
    }
}
