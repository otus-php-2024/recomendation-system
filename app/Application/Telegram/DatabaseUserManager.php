<?php

declare(strict_types=1);

namespace App\Application\Telegram;

use App\Domain\Models\Chat;
use App\Domain\Models\User;
use App\Domain\ValueObject\Telegram\ChatId;

class DatabaseUserManager
{
    public function getByChatId(ChatId $chatId): ?User
    {
        $chat = Chat::where('telegram_chat_id', $chatId->getChatId())->first();

        if ($chat === null) {
            return null;
        }

        return User::where('id', $chat->user_id)->first();
    }
}
