<?php

declare(strict_types=1);

namespace App\Application\Telegram\Start;

use App\Domain\Models\User;
use App\Domain\ValueObject\Telegram\ChatId;
use App\Domain\ValueObject\Telegram\UserId;
use Illuminate\Support\Facades\DB;
use Throwable;

class DatabaseChatCreator
{
    /**
     * @throws Throwable
     */
    public function create(UserId $userId, ChatId $chatId): void
    {
        Db::beginTransaction();

        try {
            // todo index
            $user = User::firstOrCreate([
                'telegram_id' => $userId->getUserId()
            ]);

            $chat = $user->chats()->firstOrCreate([
                'telegram_chat_id' => $chatId->getChatId()
            ]);

            $status = $chat->chatStatus();

            if (!empty($status)) {
                $status->delete();
            }

            Db::commit();
        } catch (Throwable $e) {
            Db::rollBack();
            throw $e;
        }
    }
}
