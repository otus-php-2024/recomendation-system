<?php

declare(strict_types=1);

namespace App\Application\Telegram;

use App\Application\Exceptions\TelegramException as AppTelegramException;
use App\Domain\ValueObject\Telegram\ChatId;
use App\Domain\ValueObject\Telegram\InlineButton;
use App\Domain\ValueObject\Telegram\PhotoPath;
use App\Domain\ValueObject\Telegram\SendMessageText;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;

class MessageSender
{
    /**
     * @throws TelegramException
     */
    public function send(ChatId $chatId, SendMessageText $sendMessageText): void
    {
        $data = [
            'chat_id' => $chatId->getChatId(),
            'text' => $sendMessageText->getText(),
        ];

        if ($sendMessageText->getParseMode() !== null) {
            $data['parse_mode'] = $sendMessageText->getParseMode();
        }

        Request::sendMessage($data);
    }

    /**
     * @param ChatId $chatId
     * @param SendMessageText $sendMessageText
     * @param PhotoPath $photoPath
     * @param InlineButton[] $buttons
     * @throws AppTelegramException
     * @throws TelegramException
     */
    public function sendPoster(
        ChatId $chatId,
        SendMessageText $sendMessageText,
        PhotoPath $photoPath,
        array $buttons
    ): void
    {
        if (empty($buttons)) {
            throw AppTelegramException::inlineButtonsIsEmpty();
        }

        $data = [
            'chat_id' => $chatId->getChatId(),
            'caption' => $sendMessageText->getText(),
            'photo' => Request::encodeFile($photoPath->getPath()),
            'reply_markup' => [
                'inline_keyboard' => [
                    array_map(static fn(InlineButton $button) => [
                        'text' => $button->getText(),
                        'callback_data' => $button->getCallbackData(),
                    ], $buttons)
                ]
            ]
        ];

        if ($sendMessageText->getParseMode() !== null) {
            $data['parse_mode'] = $sendMessageText->getParseMode();
        }

        Request::sendPhoto($data);
    }

    /**
     * @param ChatId $chatId
     * @param SendMessageText $message
     * @param InlineButton[] $buttons
     * @return void
     * @throws AppTelegramException
     * @throws TelegramException
     */
    public function sendMessageWithInlineButtons(ChatId $chatId, SendMessageText $message, array $buttons): void
    {
        if (empty($buttons)) {
            throw AppTelegramException::inlineButtonsIsEmpty();
        }

        $data = [
            'chat_id' => $chatId->getChatId(),
            'text' => $message->getText(),
            'reply_markup' => [
                'inline_keyboard' => [
                    array_map(static fn(InlineButton $button) => [
                        'text' => $button->getText(),
                        'callback_data' => $button->getCallbackData(),
                    ], $buttons)
                ]
            ]
        ];

        if ($message->getParseMode() !== null) {
            $data['parse_mode'] = $message->getParseMode();
        }

        Request::sendMessage($data);
    }
}
