<?php

declare(strict_types=1);

namespace App\Application\Telegram;

use App\Application\Exceptions\TelegramException;
use App\Domain\Models\Movie;
use App\Domain\ValueObject\Telegram\MovieCard;
use App\Domain\ValueObject\Telegram\PhotoPath;

class MovieCardGetter
{
    public const int MAX_LENGTH = 1024;

    /**
     * @throws TelegramException
     */
    public function getMarkdown(Movie $movie): MovieCard
    {
        $publishedYear = date('Y', strtotime($movie->published_date));
        $ourRating = round($movie->ratings->avg('rating'), 1);
        $movieCard = <<<CARD
        *Title:* {$movie->title}

        IMDb: [$movie->imdb_rating]($movie->source_url) | Our: {$ourRating}
        Year: $publishedYear
        Trailer: [watch]($movie->trailer_url)
        Director(s): {$movie->directors->implode('name', ', ')}
        Actors: {$movie->actors->implode('name', ', ')}
        Genres: {$movie->genres->implode('genre', ', ')}

        {$movie->description}

        _To rate the movie, click on the button below_
        CARD;

        if (strlen($movieCard) > self::MAX_LENGTH) {
            throw TelegramException::movieCardIsTooLong($movieCard);
        }

        return new MovieCard($movieCard, new PhotoPath($movie->poster_path));
    }
}
