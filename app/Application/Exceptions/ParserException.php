<?php

declare(strict_types=1);

namespace App\Application\Exceptions;

use Exception;
use JsonException;
use Throwable;

class ParserException extends Exception
{
    private const int SCHEMA_NOT_FOUND = 1;
    private const int SCHEMA_END_TAG_NOT_FOUND = 2;
    private const int MOVIE_NAME_IS_EMPTY = 3;
    private const int POSTER_IS_EMPTY = 4;
    private const int POSTER_NOT_VALID_URL = 5;
    private const int POSTER_URL_IS_NOT_IMAGE = 6;
    private const int DESCRIPTION_IS_EMPTY = 7;
    private const int RATING_IS_NOT_VALID = 8;
    private const int GENRE_IS_EMPTY = 9;
    private const int KEYWORDS_IS_EMPTY = 10;
    private const int TRAILER_IS_EMPTY = 11;
    private const int TRAILER_IS_NOT_VALID_URL = 12;
    private const int DIRECTOR_NAME_IS_EMPTY = 13;
    private const int DIRECTOR_URL_IS_EMPTY = 14;
    private const int DIRECTOR_URL_IS_NOT_VALID = 15;
    private const int ACTOR_NAME_IS_EMPTY = 16;
    private const int ACTOR_URL_IS_EMPTY = 17;
    private const int ACTOR_URL_IS_NOT_VALID = 18;
    private const int GENRES_IS_EMPTY = 19;
    private const int ACTORS_IS_EMPTY = 20;
    private const int DIRECTORS_IS_EMPTY = 21;
    private const int GENRE_CLASS_IS_NOT_VALID = 22;
    private const int ACTOR_CLASS_IS_NOT_VALID = 23;
    private const int DIRECTOR_CLASS_IS_NOT_VALID = 24;
    private const int SCHEMA_VALIDATION_ERROR = 25;

    private array $data;

    /**
     * @throws JsonException
     */
    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null, array $data = [])
    {
        if (!empty($data)) {
            $message .= ' ' . json_encode($data, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT);
        }

        parent::__construct($message, $code, $previous);

        $this->data = $data;
    }


    public static function schemaNotFound(): self
    {
        return new self('Schema not found', self::SCHEMA_NOT_FOUND);
    }

    public static function schemaEndTagNotFound(): self
    {
        return new self('Schema end tag not found', self::SCHEMA_END_TAG_NOT_FOUND);
    }

    public static function movieNameIsEmpty(): self
    {
        return new self('Movie name is empty', self::MOVIE_NAME_IS_EMPTY);
    }

    public static function posterIsEmpty(): self
    {
        return new self('Poster is empty', self::POSTER_IS_EMPTY);
    }

    public static function posterIsNotValidUrl(string $poster): self
    {
        return new self(sprintf('Poster "%s" is not valid url', $poster), self::POSTER_NOT_VALID_URL);
    }

    public static function posterUrlIsNotImage(string $poster): self
    {
        return new self(sprintf('Poster url "%s" is not image', $poster), self::POSTER_URL_IS_NOT_IMAGE);
    }

    public static function descriptionIsEmpty(): self
    {
        return new self('Description is empty', self::DESCRIPTION_IS_EMPTY);
    }

    public static function ratingIsNotValid(float $rating): self
    {
        return new self(sprintf('Rating "%s" is not valid', $rating), self::RATING_IS_NOT_VALID);
    }

    public static function genreIsEmpty(): self
    {
        return new self('Genre is empty', self::GENRE_IS_EMPTY);
    }

    public static function keywordsIsEmpty(): self
    {
        return new self('Keywords is empty', self::KEYWORDS_IS_EMPTY);
    }

    public static function trailerIsEmpty(): self
    {
        return new self('Trailer is empty', self::TRAILER_IS_EMPTY);
    }

    public static function trailerIsNotValidUrl(): self
    {
        return new self('Trailer is not valid url', self::TRAILER_IS_NOT_VALID_URL);
    }

    public static function directorNameIsEmpty(): self
    {
        return new self('Director name is empty', self::DIRECTOR_NAME_IS_EMPTY);
    }

    public static function directorUrlIsEmpty(): self
    {
        return new self('Director url is empty', self::DIRECTOR_URL_IS_EMPTY);
    }

    public static function directorUrlIsNotValid(): self
    {
        return new self('Director url is not valid', self::DIRECTOR_URL_IS_NOT_VALID);
    }

    public static function actorNameIsEmpty(): self
    {
        return new self('Actor name is empty', self::ACTOR_NAME_IS_EMPTY);
    }

    public static function actorUrlIsEmpty(): self
    {
        return new self('Actor url is empty', self::ACTOR_URL_IS_EMPTY);
    }

    public static function actorUrlIsNotValid(): self
    {
        return new self('Actor url is not valid', self::ACTOR_URL_IS_NOT_VALID);
    }

    public static function genresIsEmpty(): self
    {
        return new self('Genres is empty', self::GENRES_IS_EMPTY);
    }

    public static function actorsIsEmpty(): self
    {
        return new self('Actors is empty', self::ACTORS_IS_EMPTY);
    }

    public static function directorsIsEmpty(): self
    {
        return new self('Directors is empty', self::DIRECTORS_IS_EMPTY);
    }

    public static function genreClassIsNotValid(mixed $genre): self
    {
        return new self(sprintf('Genre class "%s" is not valid', self::getType($genre)), self::GENRE_CLASS_IS_NOT_VALID);
    }

    public static function actorClassIsNotValid(mixed $actor): self
    {
        return new self(sprintf('Actor class "%s" is not valid', self::getType($actor)), self::ACTOR_CLASS_IS_NOT_VALID);
    }

    private static function getType(mixed $value): string
    {
        $type = gettype($value);

        if ($type === 'object') {
            $type = get_class($value);
        }

        return $type;
    }

    public static function directorClassIsNotValid(mixed $director): self
    {
        return new self(sprintf('Director class "%s" is not valid', self::getType($director)), self::DIRECTOR_CLASS_IS_NOT_VALID);
    }

    public function getData(): array
    {
        return $this->data;
    }

    public static function validationError(array $errors): self
    {
        return new self('Schema validation error', self::SCHEMA_VALIDATION_ERROR, null, $errors);
    }
}
