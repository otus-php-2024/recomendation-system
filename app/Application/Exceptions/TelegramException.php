<?php

declare(strict_types=1);

namespace App\Application\Exceptions;

use App\Domain\ValueObject\Telegram\ChatId as ChatIdVO;
use Exception;

class TelegramException extends Exception
{
    private const int CHAT_STATUS_MUST_BE_GREATER_THAN_ZERO = 1;
    private const int CHAT_STATUS_IS_NOT_VALID = 2;
    private const int USER_ID_MUST_BE_GREATER_THAN_ZERO = 3;
    private const int CAN_NOT_FIND_CHAT = 4;
    private const int UNKNOWN_COMMAND = 5;
    private const int SEND_MESSAGE_TEXT_CANNOT_BE_EMPTY = 6;
    private const int PARSE_MODE_CANNOT_BE_EMPTY = 7;
    private const int PARSE_MODE_IS_NOT_VALID = 8;
    private const int MOVIE_WAS_NOT_FOUND_IN_DATABASE = 9;
    private const int RECOMMENDATIONS_NOT_FOUND_FOR_CHAT = 10;
    private const int MOVIE_CARD_IS_EMPTY = 11;
    private const int MOVIE_CARD_IS_TOO_LONG = 12;
    private const int POSTER_PATH_IS_EMPTY = 13;
    private const int BUTTON_TEXT_IS_EMPTY = 14;
    private const int INLINE_BUTTONS_IS_EMPTY = 15;
    private const int GET_UPDATES_FAILED = 16;

    public static function chatStatusMustBeGreaterThanZero(int $chatId): self
    {
        return new self(
            "Chat status must be greater than zero. '$chatId' provided",
            self::CHAT_STATUS_MUST_BE_GREATER_THAN_ZERO
        );
    }

    public static function chatStatusIsNotValid(int $status): self
    {
        return new self(
            "Chat status is not valid. '$status' provided",
            self::CHAT_STATUS_IS_NOT_VALID
        );
    }

    public static function userIdMustBeGreaterThanZero(int $userId): self
    {
        return new self(
            "User ID must be greater than zero. '$userId' provided",
            self::USER_ID_MUST_BE_GREATER_THAN_ZERO
        );
    }

    public static function cantFindChat(int $getChatId): self
    {
        return new self(
            "Can't find chat with chat_id: '$getChatId'",
            self::CAN_NOT_FIND_CHAT
        );
    }

    public static function unknownCommand(?string $botCommand): self
    {
        return new self(
            "Unknown command: '$botCommand'",
            self::UNKNOWN_COMMAND
        );
    }

    public static function sendMessageTextCannotBeEmpty(): self
    {
        return new self(
            "Send message text cannot be empty",
            self::SEND_MESSAGE_TEXT_CANNOT_BE_EMPTY
        );
    }

    public static function parseModeCannotBeEmpty(): self
    {
        return new self(
            "Parse mode cannot be empty",
            self::PARSE_MODE_CANNOT_BE_EMPTY
        );
    }

    public static function parseModeIsNotValid(string $parseMode): self
    {
        return new self(
            "Parse mode is not valid. '$parseMode' provided",
            self::PARSE_MODE_IS_NOT_VALID
        );
    }

    public static function movieNotFoundInDatabase(int $getMovieId): self
    {
        return new self("Movie not found by id '$getMovieId'", self::MOVIE_WAS_NOT_FOUND_IN_DATABASE);
    }

    /**
     * @param ChatIdVO $chatId
     * @return self
     */
    public static function recommendationsNotFound(ChatIdVO $chatId): self
    {
        return new self("Recommendations not found for chat id '{$chatId->getChatId()}'", self::RECOMMENDATIONS_NOT_FOUND_FOR_CHAT);
    }

    public static function movieCardIsEmpty(): self
    {
        return new self("Movie card is empty", self::MOVIE_CARD_IS_EMPTY);
    }

    public static function movieCardIsTooLong(string $movieCard): self
    {
        return new self("Movie card is too long" . PHP_EOL . PHP_EOL . $movieCard, self::MOVIE_CARD_IS_TOO_LONG);
    }

    public static function posterPathIsEmpty()
    {
        return new self("Poster path is empty", self::POSTER_PATH_IS_EMPTY);
    }

    public static function buttonTextIsEmpty()
    {
        return new self("Button text is empty", self::BUTTON_TEXT_IS_EMPTY);
    }

    public static function inlineButtonsIsEmpty(): self
    {
        return new self("Inline buttons is empty", self::INLINE_BUTTONS_IS_EMPTY);
    }

    public static function getUpdatesFailed(string $printError)
    {
        return new self("Get updates failed: $printError", self::GET_UPDATES_FAILED);
    }
}
