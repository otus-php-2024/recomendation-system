<?php

declare(strict_types=1);

namespace App;

use App\Domain\ValueObject\RecommendationSystem\Movie as MovieVO;
use App\Domain\ValueObject\RecommendationSystem\MovieRating;
use App\Domain\ValueObject\RecommendationSystem\UserRatings;
use Exception;
use Throwable;
use MathPHP\LinearAlgebra\Matrix;

class AppException extends Exception
{
    private const int TARGET_USER_ID_MUST_BE_GREATER_THAN_0 = 1;
    private const int MOVIE_ID_MUST_BE_GREATER_THAN_0 = 2;
    private const int RATING_MUST_BE_IN_RANGE = 3;
    private const int USER_ID_MUST_BE_GREATER_THAN_0 = 4;
    private const int INVALID_MOVIE_RATING_VALUE = 5;
    private const int INVALID_USER_RATINGS_VALUE = 6;
    private const int SCORE_MUST_BE_GREATER_OR_EQUAL_THAN_0 = 7;
    private const int USERS_IDS_CANNOT_BE_EMPTY = 8;
    private const int MOVIES_IDS_CANNOT_BE_EMPTY = 9;
    private const int USERS_IDS_COUNT_MUST_BE_EQUAL_TO_MATRIX_ROWS_COUNT = 10;
    private const int MOVIES_IDS_COUNT_MUST_BE_EQUAL_TO_MATRIX_COLUMNS_COUNT = 11;
    private const int MATRIX_LIKE_DATA_CANNOT_BE_EMPTY = 12;
    private const int MATRIX_MUST_BE_NUMERIC_MATRIX = 13;
    private const int TARGET_USER_NOT_FOUND_IN_MATRIX = 14;
    private const int MOVIE_ID_NOT_FOUND_IN_MATRIX = 15;
    private const int TEST_MESSAGE_CANNOT_BE_EMPTY = 16;
    public const int VALIDATION_ERROR = 17;
    private const int MOVIE_URL_IS_NOT_VALID_URL = 18;
    private const int POSTER_STORAGE_PATH_IS_EMPTY = 19;
    private const int CANNOT_DOWNLOAD_POSTER_IMAGE = 20;
    private const int MOVIE_WAS_NOT_FOUND_IN_DATABASE_AFTER_ADD_MOVIE = 21;
    private const int USER_NOT_FOUND_BY_CHAT_ID = 22;

    private array $data;

    /**
     * @throws \JsonException
     */
    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null, array $data = [])
    {
        if (!empty($data)) {
            $message .= ' ' . json_encode($data, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT);
        }

        parent::__construct($message, $code, $previous);

        $this->data = $data;
    }

    public static function targetUserIdMustBeGreaterThan0(): self
    {
        return new self('User id must be greater than 0', self::TARGET_USER_ID_MUST_BE_GREATER_THAN_0);
    }

    public static function movieIdMustBeGreaterThan0(): self
    {
        return new self('Movie id must be greater than 0', self::MOVIE_ID_MUST_BE_GREATER_THAN_0);
    }

    public static function ratingMustBeInRange(): self
    {
        return new self('Rating must be in range 1-5', self::RATING_MUST_BE_IN_RANGE);
    }

    public static function userIdMustBeGreaterThan0(): self
    {
        return new self('User id must be greater than 0', self::USER_ID_MUST_BE_GREATER_THAN_0);
    }

    public static function invalidMovieRatingValue(mixed $value): self
    {
        $type = gettype($value);

        if ($type === 'object') {
            $type = get_class($value);
        }

        return new self('Value must be instance of ' . MovieRating::class . ', ' . $type . ' given', self::INVALID_MOVIE_RATING_VALUE);
    }

    public static function invalidUserRatingsValue(mixed $value): self
    {
        $type = gettype($value);

        if ($type === 'object') {
            $type = get_class($value);
        }

        return new self('Value must be instance of ' . UserRatings::class . ', ' . $type . ' given', self::INVALID_USER_RATINGS_VALUE);
    }

    public static function scoreMustBeGreaterOrEqualThan0(): self
    {
        return new self('Score must be greater or equal then 0', self::SCORE_MUST_BE_GREATER_OR_EQUAL_THAN_0);
    }

    public static function testMessageCannotBeEmpty(): self
    {
        return new self('Test message cannot be empty', self::TEST_MESSAGE_CANNOT_BE_EMPTY);
    }

    public static function usersIdsCannotBeEmpty(): self
    {
        return new self('Users ids cannot be empty', self::USERS_IDS_CANNOT_BE_EMPTY);
    }

    public static function moviesIdsCannotBeEmpty(): self
    {
        return new self('Movies ids cannot be empty', self::MOVIES_IDS_CANNOT_BE_EMPTY);
    }

    public static function usersIdsCountMustBeEqualToMatrixRowsCount(): self
    {
        return new self('Users ids count must be equal to matrix rows count', self::USERS_IDS_COUNT_MUST_BE_EQUAL_TO_MATRIX_ROWS_COUNT);
    }

    public static function MoviesIdsCountMustBeEqualToMatrixColumnsCount(): self
    {
        return new self('Movies ids count must be equal to matrix columns count', self::MOVIES_IDS_COUNT_MUST_BE_EQUAL_TO_MATRIX_COLUMNS_COUNT);
    }

    public static function matrixLikeDataCannotBeEmpty(): self
    {
        return new self('Matrix like data cannot be empty', self::MATRIX_LIKE_DATA_CANNOT_BE_EMPTY);
    }

    public static function matrixMustBeNumericMatrix(Matrix $matrix): self
    {
        return new self('Matrix must be numeric matrix, ' . get_class($matrix) . ' given', self::MATRIX_MUST_BE_NUMERIC_MATRIX);
    }

    public static function targetUserNotFoundInMatrix(int $userId): self
    {
        return new self('Target user with id ' . $userId . ' not found in matrix', self::TARGET_USER_NOT_FOUND_IN_MATRIX);
    }

    public static function movieIdNotFoundInMatrix(int|string $moviePosition): self
    {
        return new self("Movie id not found in matrix by position '$moviePosition'", self::MOVIE_ID_NOT_FOUND_IN_MATRIX);
    }

    public static function validationError(array $errors): self
    {
        return new self('Validation error', self::VALIDATION_ERROR, null, $errors);
    }

    public static function movieUrlIsNotValidUrl(): self
    {
        return new self('Movie url is not valid url', self::MOVIE_URL_IS_NOT_VALID_URL);
    }

    public static function posterStoragePathIsEmpty(): self
    {
        return new self('Poster storage path is empty', self::POSTER_STORAGE_PATH_IS_EMPTY);
    }

    public static function cannotDownloadPoster(string $url): self
    {
        return new self('Cannot download poster from url: ' . $url, self::CANNOT_DOWNLOAD_POSTER_IMAGE);
    }

    public static function movieWasNotFoundInDatabaseAfterAddMovie(MovieVO $movie): self
    {
        return new self("Cannot find movie '{$movie->getMovieId()}' after add movie", self::MOVIE_WAS_NOT_FOUND_IN_DATABASE_AFTER_ADD_MOVIE);
    }

    public static function userNotFoundByChatId(int $getChatId): self
    {
        return new self("User not found by chat id: $getChatId", self::USER_NOT_FOUND_BY_CHAT_ID);
    }

    public function getData(): array
    {
        return $this->data;
    }
}
