<?php

declare(strict_types=1);

namespace App\Infrastructure\Providers;

use App\Application\Parser\Crawler;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class CrawlerServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(Crawler::class, function () {
            return new Crawler(
                new Client(['base_uri' => 'https://api.crawlbase.com']),
                config('services.crawler.token'),
            );
        });
    }
}
