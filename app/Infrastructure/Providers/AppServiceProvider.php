<?php

namespace App\Infrastructure\Providers;

use App\Application\RecommendationSystem\UsersRatingsIterator;
use App\Domain\UserRatingsIteratorInterface;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(
            UserRatingsIteratorInterface::class,
            UsersRatingsIterator::class
        );

        $this->app->bind(
            ValidatorInterface::class,
            function () {
                return Validation::createValidator();
            }
        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
