<?php

declare(strict_types=1);

namespace App\Infrastructure\Providers;

use Illuminate\Support\ServiceProvider;
use Longman\TelegramBot\Telegram;

class TelegramApiServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(Telegram::class, function () {
            $telegramService = new Telegram(
                config('services.telegram.token'),
                config('services.telegram.bot_name')
            );

            $telegramService->useGetUpdatesWithoutDatabase();
            $telegramService->addCommandsPath(base_path('app/Infrastructure/Telegram/Command'));

            return $telegramService;
        });
    }
}
