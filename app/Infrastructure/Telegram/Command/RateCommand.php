<?php

declare(strict_types=1);

namespace App\Infrastructure\Telegram\Command;

use App\Application\Request\Telegram\RateMovieRequest;
use App\Application\UseCase\Telegram\RateMovieUseCase;
use App\Domain\Telegram\ApiCallbacks;
use Illuminate\Support\Facades\App;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

class RateCommand extends UserCommand
{
    protected $name = 'Rate';
    protected $description = 'Rate film';
    protected $usage = '/' . ApiCallbacks::RATE;

    public function execute(): ServerResponse
    {
        $callbackQuery = $this->getCallbackQuery();

        $chatId = $callbackQuery->getMessage()->getChat()->getId();
        $data = $callbackQuery->getData();
        $data = json_decode($data, true, 512, JSON_THROW_ON_ERROR);
        $movie = $data['movie'] ?? null;
        $rating = $data['rating'] ?? null;

        /** @var RateMovieUseCase $service */
        $service = App::make(RateMovieUseCase::class);
        $dto = new RateMovieRequest($chatId, $movie, $rating);

        $service->execute($dto);

        return Request::emptyResponse();
    }
}
