<?php

declare(strict_types=1);

namespace App\Infrastructure\Telegram\Command;

use App\AppException;
use App\Application\Request\Telegram\GetRecommendationCommandRequest;
use App\Application\UseCase\Telegram\GetRecommendationCommandUseCase;
use App\Domain\Telegram\ApiCommands;
use Illuminate\Support\Facades\App;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;

class RecommendCommand extends UserCommand
{
    protected $name = 'Recommend';
    protected $description = 'recommend films for the user';
    protected $usage = ApiCommands::RECOMMEND_FILM;

    /**
     * @return ServerResponse
     * @throws AppException
     * @throws TelegramException
     * @throws \App\Application\Exceptions\TelegramException
     * @throws \JsonException
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $chatId = $message->getChat()->getId();

        /** @var GetRecommendationCommandUseCase $service */
        $service = App::make(GetRecommendationCommandUseCase::class);
        $request = new GetRecommendationCommandRequest($chatId);
        $service->execute($request);

        return Request::emptyResponse();
    }
}
