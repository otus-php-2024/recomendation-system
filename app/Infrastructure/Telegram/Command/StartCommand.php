<?php

declare(strict_types=1);

namespace App\Infrastructure\Telegram\Command;

use App\Application\Request\Telegram\StartCommandRequest;
use App\Application\UseCase\Telegram\StartCommandUseCase;
use App\Domain\Telegram\ApiCommands;
use Illuminate\Support\Facades\App;
use Longman\TelegramBot\Commands\UserCommands\StartCommand as BaseStartCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;

class StartCommand extends BaseStartCommand
{
    protected $usage = ApiCommands::START;

    /**
     * @throws TelegramException
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $chatId = $message->getChat()->getId();
        $userId = $message->getFrom()->getId();

        /** @var StartCommandUseCase $service */
        $service = App::make(StartCommandUseCase::class);
        $request = new StartCommandRequest($userId, $chatId);
        $response = $service->execute($request);

        return Request::sendMessage($response->toArray());
    }
}
