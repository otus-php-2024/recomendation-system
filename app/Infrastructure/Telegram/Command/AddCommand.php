<?php

declare(strict_types=1);

namespace App\Infrastructure\Telegram\Command;

use App\AppException;
use App\Application\Request\Telegram\AddMovieCommandRequest;
use App\Application\UseCase\Telegram\AddMovieCommandUseCase;
use App\Domain\Telegram\ApiCommands;
use Illuminate\Support\Facades\App;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;

class AddCommand extends UserCommand
{
    protected $name = 'Add';
    protected $description = 'Add new film to the database';
    protected $usage = ApiCommands::ADD_FILM;

    /**
     * @throws AppException
     * @throws TelegramException
     * @throws \App\Application\Exceptions\TelegramException
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $chatId = $message->getChat()->getId();

        /** @var AddMovieCommandUseCase $service */
        $service = App::make(AddMovieCommandUseCase::class);
        $request = new AddMovieCommandRequest($chatId);
        $response = $service->execute($request);

        return Request::sendMessage($response->toArray());
    }
}
