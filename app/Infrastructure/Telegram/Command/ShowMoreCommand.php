<?php

declare(strict_types=1);

namespace App\Infrastructure\Telegram\Command;

use App\AppException;
use App\Application\Exceptions\TelegramException;
use App\Application\Request\Telegram\GetListOfRecommendationsRequest;
use App\Application\UseCase\Telegram\GetListOfRecommendationsUseCase;
use App\Domain\Telegram\ApiCallbacks;
use Illuminate\Support\Facades\App;
use JsonException;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

class ShowMoreCommand extends UserCommand
{
    protected $name = 'Show more';
    protected $description = 'Show more films';
    protected $usage = '/' . ApiCallbacks::SHOW_MORE;

    /**
     * @throws AppException
     * @throws TelegramException
     * @throws \Longman\TelegramBot\Exception\TelegramException
     * @throws JsonException
     */
    public function execute(): ServerResponse
    {
        $callbackQuery = $this->getCallbackQuery();

        $chatId = $callbackQuery->getMessage()->getChat()->getId();
        $data = $callbackQuery->getData();
        $data = json_decode($data, true, 512, JSON_THROW_ON_ERROR);
        $offset = is_numeric($data['offset']) ? (int)$data['offset'] :  0;

        /** @var GetListOfRecommendationsUseCase $service */
        $service = App::make(GetListOfRecommendationsUseCase::class);
        $dto = new GetListOfRecommendationsRequest($chatId, $offset);

        $service->execute($dto);

        return Request::emptyResponse();
    }
}
