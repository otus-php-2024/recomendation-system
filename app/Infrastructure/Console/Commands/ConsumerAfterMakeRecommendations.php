<?php

declare(strict_types=1);

namespace App\Infrastructure\Console\Commands;

use App\Application\UseCase\Queue\AfterMakeRecommendationsUseCase;
use Illuminate\Console\Command;

class ConsumerAfterMakeRecommendations extends Command
{
    protected $signature = 'app:consume:after-make-recommendations';
    protected $description = 'After make recommendations from queue';

    private AfterMakeRecommendationsUseCase $useCase;

    public function __construct(AfterMakeRecommendationsUseCase $useCase)
    {
        $this->useCase = $useCase;
        parent::__construct();
    }

    /**
     * @throws \ErrorException
     */
    public function handle(): void
    {
        $this->useCase->execute();
    }
}
