<?php

declare(strict_types=1);

namespace App\Infrastructure\Console\Commands;

use App\Application\UseCase\Queue\AddMovieUseCase;
use Illuminate\Console\Command;

class ConsumerAddMovie extends Command
{
    protected $signature = 'app:consume:add-movie';
    protected $description = 'Add movie from queue';
    private AddMovieUseCase $useCase;

    public function __construct(AddMovieUseCase $useCase)
    {
        $this->useCase = $useCase;

        parent::__construct();
    }

    /**
     * @throws \ErrorException
     */
    public function handle(): void
    {
        $this->useCase->execute();
    }
}
