<?php

declare(strict_types=1);

namespace App\Infrastructure\Console\Commands;

use App\Application\UseCase\Queue\AfterAddMovieUseCase;
use Illuminate\Console\Command;

class ConsumerAfterAddMovie extends Command
{
    protected $signature = 'app:consume:after-add-movie';
    protected $description = 'After add movie from queue';
    private AfterAddMovieUseCase $useCase;

    public function __construct(AfterAddMovieUseCase $useCase)
    {
        $this->useCase = $useCase;
        parent::__construct();
    }

    /**
     * @throws \ErrorException
     */
    public function handle(): void
    {
        $this->useCase->execute();
    }
}
