<?php

declare(strict_types=1);

namespace App\Infrastructure\Console\Commands;

use App\AppException;
use App\Application\UseCase\Telegram\GetUpdatesUseCase;
use Illuminate\Console\Command;
use JsonException;
use Longman\TelegramBot\Exception\TelegramException;

class TelegramGetUpdates extends Command
{
    private GetUpdatesUseCase $getUpdatesUseCase;

    public function __construct(
        GetUpdatesUseCase $getUpdatesUseCase
    )
    {
        parent::__construct();

        $this->getUpdatesUseCase = $getUpdatesUseCase;
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:telegram-get-updates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get updates from telegram server';

    /**
     * Execute the console command.
     * @throws AppException
     * @throws TelegramException
     * @throws \App\Application\Exceptions\TelegramException
     * @throws JsonException
     */
    public function handle(): void
    {
        $this->getUpdatesUseCase->execute();
    }
}
