<?php

declare(strict_types=1);

namespace App\Infrastructure\Console\Commands;

use App\Application\UseCase\Queue\MakeRecommendationsUseCase;
use Illuminate\Console\Command;

class ConsumerMakeRecommendations extends Command
{
    protected $signature = 'app:consume:make-recommendations';
    protected $description = 'Make recommendations from queue';
    private MakeRecommendationsUseCase $useCase;

    public function __construct(MakeRecommendationsUseCase $useCase)
    {
        $this->useCase = $useCase;
        parent::__construct();
    }

    /**
     * @throws \ErrorException
     */
    public function handle(): void
    {
        $this->useCase->execute();
    }
}
