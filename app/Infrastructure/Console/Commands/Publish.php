<?php

declare(strict_types=1);

namespace App\Infrastructure\Console\Commands;

use App\Domain\ValueObject\PublisherTestMessageRequest;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Publish extends Command
{
    private AMQPStreamConnection $connection;

    public function __construct(AMQPStreamConnection $connection)
    {
        $this->connection = $connection;

        parent::__construct();
    }

    protected $signature = 'app:publish';
    protected $description = 'Rabbit mq publish message';

    /**
     * @throws \JsonException
     * @throws \Exception
     */
    public function handle(): void
    {
        $channel = $this->connection->channel();
        $channel->exchange_declare('test_exchange', 'direct', false, false, false);
        $channel->queue_declare('test_queue', false, false, false, false);
        $channel->queue_bind('test_queue', 'test_exchange', 'test_key');

        $massageToQueue = new PublisherTestMessageRequest('test message 3');

        $msg = new AMQPMessage(json_encode(['message' => $massageToQueue->getTestMessage()], JSON_THROW_ON_ERROR));

        $channel->basic_publish($msg, 'test_exchange', 'test_key');

        $this->info('Sent message to test_exchange / test_queue.');

        $channel->close();
        $this->connection->close();
    }
}
