<?php

namespace App\Infrastructure\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class TestRedis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:test-redis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Redis connection and operatable';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $testName = "testName";
        $testVal = "testVal";
        $redis = Redis::connection();
        $redis->set($testName, $testVal);

        if ($redis->get($testName) == $testVal) {
            $this->info("Redis test PASSED OK");
        } else {
            $this->info("Redis test FAILED");
        }
    }
}
