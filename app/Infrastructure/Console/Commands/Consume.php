<?php

declare(strict_types=1);

namespace App\Infrastructure\Console\Commands;

use App\Domain\ValueObject\ConsumerTestMassageRequest;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class Consume extends Command
{
    protected $signature = 'app:consume';
    protected $description = 'Rabbit mq consume message';
    private AMQPStreamConnection $connection;

    public function __construct(AMQPStreamConnection $connection)
    {
        $this->connection = $connection;

        parent::__construct();
    }

    /**
     * @throws \ErrorException
     */
    public function handle(): void
    {
        $channel = $this->connection->channel();
        $callback = function ($msg) {
            $this->info(' [x] Received ' . $msg->body);
            $decoded = json_decode($msg->body, true, 512, JSON_THROW_ON_ERROR);
            $message = new ConsumerTestMassageRequest($decoded['message']);

            $this->info(' [x] Message: ' . $message->getTestMessage());
        };

        $channel->queue_declare('test_queue', false, false, false, false);
        $channel->basic_consume('test_queue', '', false, true, false, false, $callback);

        $this->info(' [*] Waiting for messages. To exit press CTRL+C');

        $channel->consume();
    }
}
