<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers;

use App\Application\UseCase\TelegramBotUseCase;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;

class TelegramController extends Controller
{
    public function getUpdates(TelegramBotUseCase $telegramBotService): ResponseFactory|Response
    {
        $telegramBotService->getUpdates();

        return response('ok', 200);
    }
}
