<?php

declare(strict_types=1);

namespace App\Domain\Models;

use Database\Factories\RatingFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use HasFactory;

    protected static function newFactory(): RatingFactory
    {
        return RatingFactory::new();
    }
}
