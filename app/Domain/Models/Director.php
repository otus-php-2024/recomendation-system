<?php

declare(strict_types=1);

namespace App\Domain\Models;

use Database\Factories\DirectorFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Director extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'url'];

    protected static function newFactory(): DirectorFactory
    {
        return DirectorFactory::new();
    }
}
