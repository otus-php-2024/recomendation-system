<?php

declare(strict_types=1);

namespace App\Domain\Models;

use Database\Factories\GenreFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;

    protected $fillable = ['genre'];

    protected static function newFactory(): GenreFactory
    {
        return GenreFactory::new();
    }
}
