<?php

declare(strict_types=1);

namespace App\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class ChatStatus extends Model
{
    public const int STATUS_WAITING_FOR_FILM_URL = 1;
    public const int STATUS_ADDING_FILM = 2;

    public const array AVAILABLE_STATUSES = [
        self::STATUS_WAITING_FOR_FILM_URL,
        self::STATUS_ADDING_FILM,
    ];

    protected $fillable = [
        'status',
    ];
}
