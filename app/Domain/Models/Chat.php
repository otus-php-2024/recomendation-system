<?php

declare(strict_types=1);

namespace App\Domain\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Chat extends Model
{
    protected $fillable = [
        'telegram_chat_id'
    ];

    public function chatStatus(): HasOne
    {
        return $this->hasOne(ChatStatus::class);
    }
}
