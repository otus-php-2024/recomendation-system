<?php

namespace App\Domain\Models;

use Database\Factories\MovieFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Movie extends Model
{
    use HasFactory;

    protected $fillable = [
        'source_url',
    ];

    public function genres(): BelongsToMany
    {
        return $this->belongsToMany(Genre::class, MovieGenre::class);
    }

    public function actors(): BelongsToMany
    {
        return $this->belongsToMany(Actor::class, MovieActor::class);
    }

    public function directors(): BelongsToMany
    {
        return $this->belongsToMany(Director::class, MovieDirector::class);
    }

    public function ratings(): HasMany
    {
        return $this->hasMany(Rating::class);
    }

    protected static function newFactory(): MovieFactory
    {
        return MovieFactory::new();
    }
}
