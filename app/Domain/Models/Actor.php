<?php

declare(strict_types=1);

namespace App\Domain\Models;

use Database\Factories\ActorFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'url'];

    protected static function newFactory(): ActorFactory
    {
        return ActorFactory::new();
    }
}
