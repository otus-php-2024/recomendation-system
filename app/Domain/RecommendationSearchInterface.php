<?php

namespace App\Domain;

use App\Domain\ValueObject\RecommendationSystem\Recommendation;
use App\Domain\ValueObject\RecommendationSystem\TargetUser;

interface RecommendationSearchInterface
{
    /**
     * @param UserRatingsIteratorInterface $iterator
     * @param TargetUser $targetUser
     * @return Recommendation[]
     */
    public function search(UserRatingsIteratorInterface $iterator, TargetUser $targetUser): array;
}
