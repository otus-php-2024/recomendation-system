<?php

declare(strict_types=1);

namespace App\Domain\Telegram;

class ApiCallbacks
{
    public const string SHOW_MORE = 'show_more';
    public const string RATE = 'rate';

    public const array CALLBACKS = [
        self::SHOW_MORE,
        self::RATE,
    ];
}
