<?php

declare(strict_types=1);

namespace App\Domain\Telegram;

class ApiCommands
{
    public const string ADD_FILM = '/add';
    public const string RECOMMEND_FILM = '/recommend';
    public const string START = '/start';

    public const array COMMANDS = [
        self::ADD_FILM,
        self::RECOMMEND_FILM,
        self::START,
    ];
}
