<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Telegram;

use App\Application\Exceptions\TelegramException;

class ChatStatus
{
    public int $status;

    /**
     * @throws TelegramException
     */
    public function __construct(int $status)
    {
        $this->validate($status);

        $this->status = $status;
    }

    /**
     * @throws TelegramException
     */
    private function validate(int $status): void
    {
        if (!in_array($status, \App\Domain\Models\ChatStatus::AVAILABLE_STATUSES, true)) {
            throw TelegramException::chatStatusIsNotValid($status);
        }
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
