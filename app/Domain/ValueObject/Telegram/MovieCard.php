<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Telegram;

use App\Application\Exceptions\TelegramException;
use App\Application\Telegram\MovieCardGetter;

class MovieCard
{
    private string $movieCard;
    private PhotoPath $posterPath;

    /**
     * @throws TelegramException
     */
    public function __construct(string $movieCard, PhotoPath $posterPath)
    {
        $this->validate($movieCard);

        $this->movieCard = $movieCard;
        $this->posterPath = $posterPath;
    }

    /**
     * @throws TelegramException
     */
    private function validate(string $movieCard): void
    {
        if (empty($movieCard)) {
            throw TelegramException::movieCardIsEmpty();
        }

        if (strlen($movieCard) > MovieCardGetter::MAX_LENGTH) {
            throw TelegramException::movieCardIsTooLong($movieCard);
        }
    }

    public function getMovieCard(): string
    {
        return $this->movieCard;
    }

    public function getPosterPath(): PhotoPath
    {
        return $this->posterPath;
    }
}
