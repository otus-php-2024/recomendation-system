<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Telegram;

use App\Application\Exceptions\TelegramException;

class InlineButton
{
    private string $text;
    private string $callbackData;

    /**
     * @throws TelegramException
     */
    public function __construct(string $text, string $callbackData = '')
    {
        $this->validate($text);

        $this->text = $text;
        $this->callbackData = $callbackData;
    }

    /**
     * @throws TelegramException
     */
    private function validate(string $text): void
    {
        if (empty($text)) {
            throw TelegramException::buttonTextIsEmpty();
        }
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getCallbackData(): string
    {
        return $this->callbackData;
    }
}
