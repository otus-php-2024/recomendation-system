<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Telegram;

use App\Application\Exceptions\TelegramException;

class PhotoPath
{
    private string $path;

    /**
     * @throws TelegramException
     */
    public function __construct(string $path)
    {
        $this->validate($path);

        $this->path = $path;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @throws TelegramException
     */
    private function validate(string $path)
    {
        if ($path === '') {
            throw TelegramException::posterPathIsEmpty();
        }
    }
}
