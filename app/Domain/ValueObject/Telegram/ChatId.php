<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Telegram;

use App\Application\Exceptions\TelegramException;

class ChatId
{
    private int $chatId;

    /**
     * @throws TelegramException
     */
    public function __construct(int $chatId)
    {
        $this->validate($chatId);

        $this->chatId = $chatId;
    }

    /**
     * @throws TelegramException
     */
    private function validate(int $chatId): void
    {
        if ($chatId <= 0) {
            throw TelegramException::chatStatusMustBeGreaterThanZero($chatId);
        }
    }

    public function getChatId(): int
    {
        return $this->chatId;
    }
}
