<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Telegram;

use App\Application\Exceptions\TelegramException;

class UserId
{
    private int $userId;

    public function __construct(int $userId)
    {
        $this->validate($userId);

        $this->userId = $userId;
    }

    private function validate(int $userId): void
    {
        if ($userId <= 0) {
            throw TelegramException::userIdMustBeGreaterThanZero($userId);
        }
    }

    public function getUserId(): int
    {
        return $this->userId;
    }
}
