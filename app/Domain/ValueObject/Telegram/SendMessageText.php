<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Telegram;

use App\Application\Exceptions\TelegramException;

class SendMessageText
{
    public const PARSE_MODE_HTML = 'HTML';
    public const PARSE_MODE_MARKDOWN = 'Markdown';

    private string $text;
    private ?string $parseMode;

    public function __construct(string $text, ?string $parseMode = null)
    {
        $this->validate($text, $parseMode);

        $this->text = $text;
        $this->parseMode = $parseMode;
    }

    /**
     * @throws TelegramException
     */
    private function validate(string $text, ?string $parseMode): void
    {
        if ($text === '') {
            throw TelegramException::sendMessageTextCannotBeEmpty();
        }

        if ($parseMode === '') {
            throw TelegramException::parseModeCannotBeEmpty();
        }

        if ($parseMode !== null && !in_array($parseMode, [self::PARSE_MODE_HTML, self::PARSE_MODE_MARKDOWN])) {
            throw TelegramException::parseModeIsNotValid($parseMode);
        }
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getParseMode(): ?string
    {
        return $this->parseMode;
    }
}
