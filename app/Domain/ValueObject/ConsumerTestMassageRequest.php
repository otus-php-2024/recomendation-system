<?php

declare(strict_types=1);

namespace App\Domain\ValueObject;

use App\AppException;

class ConsumerTestMassageRequest
{
    private string $testMessage;

    /**
     * @throws AppException
     */
    public function __construct(string $testMessage)
    {
        $this->validate($testMessage);

        $this->testMessage = $testMessage;
    }

    public function getTestMessage(): string
    {
        return $this->testMessage;
    }

    /**
     * @throws AppException
     */
    private function validate(string $testMessage): void
    {
        if ($testMessage === '') {
            throw AppException::testMessageCannotBeEmpty();
        }
    }
}
