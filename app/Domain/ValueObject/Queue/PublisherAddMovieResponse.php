<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Queue;

use App\Domain\ValueObject\RecommendationSystem\Movie;
use App\Domain\ValueObject\Telegram\ChatId;

class PublisherAddMovieResponse
{
    private Movie $movie;
    private ChatId $chatId;

    public function __construct(Movie $movieId, ChatId $chatId)
    {
        $this->movie = $movieId;
        $this->chatId = $chatId;
    }

    public function getMovie(): Movie
    {
        return $this->movie;
    }

    public function getChatId(): ChatId
    {
        return $this->chatId;
    }
}
