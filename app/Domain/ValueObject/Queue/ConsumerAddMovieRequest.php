<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Queue;

use App\Domain\ValueObject\AddNewMovie\MovieUrl;

class ConsumerAddMovieRequest
{
    private MovieUrl $movieUrl;

    public function __construct(MovieUrl $url)
    {
        $this->movieUrl = $url;
    }

    public function getMovieUrl(): MovieUrl
    {
        return $this->movieUrl;
    }
}
