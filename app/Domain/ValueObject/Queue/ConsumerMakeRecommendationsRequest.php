<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Queue;

use App\Domain\ValueObject\Telegram\ChatId;

class ConsumerMakeRecommendationsRequest
{
    private ChatId $chatId;

    public function __construct(ChatId $chatId)
    {
        $this->chatId = $chatId;
    }

    public function getChatId(): ChatId
    {
        return $this->chatId;
    }
}
