<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Queue;

use App\Domain\ValueObject\AddNewMovie\MovieUrl;
use App\Domain\ValueObject\Telegram\ChatId;

class PublisherAfterAddMovieRequest
{
    private ChatId $chatId;
    private MovieUrl $movieUrl;

    public function __construct(ChatId $chatId, MovieUrl $url)
    {
        $this->chatId = $chatId;
        $this->movieUrl = $url;
    }

    public function getChatId(): ChatId
    {
        return $this->chatId;
    }

    public function getMovieUrl(): MovieUrl
    {
        return $this->movieUrl;
    }
}
