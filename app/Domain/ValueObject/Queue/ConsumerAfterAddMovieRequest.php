<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Queue;

use App\Domain\ValueObject\RecommendationSystem\Movie;
use App\Domain\ValueObject\Telegram\ChatId;

class ConsumerAfterAddMovieRequest
{
    private ChatId $chatId;
    private Movie $movie;

    public function __construct(ChatId $chatId, Movie $url)
    {
        $this->chatId = $chatId;
        $this->movie = $url;
    }

    public function getChatId(): ChatId
    {
        return $this->chatId;
    }

    public function getMovie(): Movie
    {
        return $this->movie;
    }
}
