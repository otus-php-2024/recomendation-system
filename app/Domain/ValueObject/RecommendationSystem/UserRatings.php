<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\RecommendationSystem;

use App\AppException;

class UserRatings
{
    private User $user;
    /** @var MovieRating[] */
    private array $ratings;

    /**
     * @param MovieRating[] $ratings
     * @throws AppException
     */
    public function __construct(User $user, array $ratings)
    {
        $this->validate($ratings);

        $this->user = $user;
        $this->ratings = $ratings;
    }

    /**
     * @throws AppException
     */
    private function validate(array $ratings): void
    {
        foreach ($ratings as $rating) {
            if (! $rating instanceof MovieRating) {
                throw AppException::invalidMovieRatingValue($rating);
            }
        }
    }

    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return MovieRating[]
     */
    public function getRatings(): array
    {
        return $this->ratings;
    }
}
