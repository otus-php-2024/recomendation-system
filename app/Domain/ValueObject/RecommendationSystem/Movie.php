<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\RecommendationSystem;

use App\AppException;

class Movie
{
    private int $movieId;

    /**
     * @throws AppException
     */
    public function __construct(int $movieId)
    {
        $this->validate($movieId);

        $this->movieId = $movieId;
    }

    public function getMovieId(): int
    {
        return $this->movieId;
    }

    /**
     * @throws AppException
     */
    private function validate(int $movieId): void
    {
        if ($movieId <= 0) {
            throw AppException::movieIdMustBeGreaterThan0();
        }
    }
}
