<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\RecommendationSystem;

class Recommendation
{
    private Movie $movie;
    private Score $score;

    public function __construct(Movie $movie, Score $score)
    {
        $this->movie = $movie;
        $this->score = $score;
    }

    public function getMovie(): Movie
    {
        return $this->movie;
    }

    public function getScore(): Score
    {
        return $this->score;
    }
}
