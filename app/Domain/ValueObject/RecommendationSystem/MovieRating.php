<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\RecommendationSystem;

class MovieRating
{
    private Movie $movie;
    private Rating $rating;

    public function __construct(Movie $movie, Rating $rating)
    {
        $this->movie = $movie;
        $this->rating = $rating;
    }

    public function getMovie(): Movie
    {
        return $this->movie;
    }

    public function getRating(): Rating
    {
        return $this->rating;
    }
}
