<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\RecommendationSystem;

use App\AppException;

class Rating
{
    private int $rating;

    /**
     * @throws AppException
     */
    public function __construct(int $rating)
    {
        $this->validate($rating);

        $this->rating = $rating;
    }

    public function getRatingValue(): int
    {
        return $this->rating;
    }

    /**
     * @throws AppException
     */
    private function validate(int $rating): void
    {
        if ($rating < 1 || $rating > 5) {
            throw AppException::ratingMustBeInRange();
        }
    }
}
