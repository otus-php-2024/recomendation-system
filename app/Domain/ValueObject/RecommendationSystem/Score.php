<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\RecommendationSystem;

use App\AppException;

class Score
{
    private float $score;

    /**
     * @throws AppException
     */
    public function __construct(float $score)
    {
        $this->validate($score);

        $this->score = $score;
    }

    /**
     * @throws AppException
     */
    private function validate(float $score): void
    {
        if ($score < 0) {
            throw AppException::scoreMustBeGreaterOrEqualThan0();
        }
    }

    public function getScoreValue(): float
    {
        return $this->score;
    }
}
