<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\RecommendationSystem;

use App\AppException;

class User
{
    private int $userId;

    /**
     * @throws AppException
     */
    public function __construct(int $userId)
    {
        $this->validate($userId);

        $this->userId = $userId;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @throws AppException
     */
    private function validate(int $userId): void
    {
        if ($userId <= 0) {
            throw AppException::userIdMustBeGreaterThan0();
        }
    }
}
