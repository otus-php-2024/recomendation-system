<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\RecommendationSystem\Vector;

use MathPHP\LinearAlgebra\NumericMatrix;

class SVD
{
    public function __construct(
        private readonly NumericMatrix $U,
        private readonly NumericMatrix $S,
        private readonly NumericMatrix $V
    ) {
    }

    public function getU(): NumericMatrix
    {
        return $this->U;
    }

    public function getS(): NumericMatrix
    {
        return $this->S;
    }

    public function getV(): NumericMatrix
    {
        return $this->V;
    }
}
