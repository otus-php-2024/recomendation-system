<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\RecommendationSystem\Vector;

use App\AppException;
use MathPHP\LinearAlgebra\Matrix;

class UsersMatrix
{
    private Matrix $matrix;
    private array $usersIds;
    private array $moviesIds;

    /**
     * @throws AppException
     */
    public function __construct(Matrix $matrix, array $usersIds, array $moviesIds)
    {
        $this->validate($matrix, $usersIds, $moviesIds);

        $this->matrix = $matrix;
        $this->usersIds = $usersIds;
        $this->moviesIds = $moviesIds;
    }

    /**
     * @throws AppException
     */
    private function validate(Matrix $matrix, array $usersIds, array $moviesIds): void
    {
        if (empty($usersIds)) {
            throw AppException::usersIdsCannotBeEmpty();
        }

        if (empty($moviesIds)) {
            throw AppException::moviesIdsCannotBeEmpty();
        }

        if ($matrix->getM() !== count($usersIds)) {
            throw AppException::usersIdsCountMustBeEqualToMatrixRowsCount();
        }

        if ($matrix->getN() !== count($moviesIds)) {
            throw AppException::MoviesIdsCountMustBeEqualToMatrixColumnsCount();
        }
    }

    public function getMatrix(): Matrix
    {
        return $this->matrix;
    }

    public function getUsersIds(): array
    {
        return $this->usersIds;
    }

    public function getMoviesIds(): array
    {
        return $this->moviesIds;
    }

    public function getUserPosition(int $userId): ?int
    {
        $result = array_search($userId, $this->usersIds, true);

        return $result === false ? null : (int) $result;
    }

    public function getMovieId(int $moviePosition): ?int
    {
        return $this->moviesIds[$moviePosition] ?? null;
    }
}
