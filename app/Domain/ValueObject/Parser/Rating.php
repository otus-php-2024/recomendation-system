<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Parser;

use App\Application\Exceptions\ParserException;

class Rating
{
    private float $rating;

    /**
     * @throws ParserException
     */
    public function __construct(float $rating)
    {
        $this->validate($rating);

        $this->rating = $rating;
    }

    public function getRating(): float
    {
        return $this->rating;
    }

    /**
     * @throws ParserException
     */
    private function validate(float $rating): void
    {
        if ($rating < 0 || $rating > 10) {
            throw ParserException::ratingIsNotValid($rating);
        }
    }
}
