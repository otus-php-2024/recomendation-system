<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Parser;

use App\Application\Exceptions\ParserException;

class Description
{
    private string $description;

    /**
     * @throws ParserException
     */
    public function __construct(string $description)
    {
        $this->validate($description);

        $this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @throws ParserException
     */
    private function validate(string $description): void
    {
        if (empty($description)) {
            throw ParserException::descriptionIsEmpty();
        }
    }
}
