<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Parser;

use App\Application\Exceptions\ParserException;

class Keywords
{
    private string $keywords;

    /**
     * @throws ParserException
     */
    public function __construct(string $keywords)
    {
        $this->validate($keywords);

        $this->keywords = $keywords;
    }

    public function getKeywords(): string
    {
        return $this->keywords;
    }

    /**
     * @throws ParserException
     */
    private function validate(string $keywords): void
    {
        if (empty($keywords)) {
            throw ParserException::keywordsIsEmpty();
        }
    }
}
