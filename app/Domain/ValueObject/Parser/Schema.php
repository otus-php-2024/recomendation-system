<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Parser;

use App\Application\Exceptions\ParserException;

class Schema
{
    private MovieName $movieName;
    private Poster $poster;
    private Description $description;
    private Rating $rating;
    /** @var Genre[] */
    private array $genres;
    private Keywords $keywords;
    private Trailer $trailer;
    /** @var Actor[] */
    private array $actors;
    /** @var Director[] */
    private array $directors;
    private PublishedDate $publishedDate;

    /**
     * @throws ParserException
     */
    public function __construct(
        MovieName   $movieName,
        Poster      $poster,
        Description $description,
        Rating      $rating,
        array       $genres,
        Keywords    $keywords,
        Trailer     $trailer,
        array       $actors,
        array       $directors,
        PublishedDate $publishedDate
    ) {
        $this->validate($genres, $actors, $directors);

        $this->movieName = $movieName;
        $this->poster = $poster;
        $this->description = $description;
        $this->rating = $rating;
        $this->genres = $genres;
        $this->keywords = $keywords;
        $this->trailer = $trailer;
        $this->actors = $actors;
        $this->directors = $directors;
        $this->publishedDate = $publishedDate;
    }

    /**
     * @throws ParserException
     */
    private function validate(array $genres, array $actors, array $directors): void
    {
        if (empty($genres)) {
            throw ParserException::genresIsEmpty();
        }

        if (empty($actors)) {
            throw ParserException::actorsIsEmpty();
        }

        if (empty($directors)) {
            throw ParserException::directorsIsEmpty();
        }

        foreach ($genres as $genre) {
            if (!$genre instanceof Genre) {
                throw ParserException::genreClassIsNotValid($genre);
            }
        }

        foreach ($actors as $actor) {
            if (!$actor instanceof Actor) {
                throw ParserException::actorClassIsNotValid($actor);
            }
        }

        foreach ($directors as $director) {
            if (!$director instanceof Director) {
                throw ParserException::directorClassIsNotValid($director);
            }
        }
    }

    public function getMovieName(): MovieName
    {
        return $this->movieName;
    }

    public function getPoster(): Poster
    {
        return $this->poster;
    }

    public function getDescription(): Description
    {
        return $this->description;
    }

    public function getRating(): Rating
    {
        return $this->rating;
    }

    public function getGenres(): array
    {
        return $this->genres;
    }

    public function getKeywords(): Keywords
    {
        return $this->keywords;
    }

    public function getTrailer(): Trailer
    {
        return $this->trailer;
    }

    public function getActors(): array
    {
        return $this->actors;
    }

    public function getDirectors(): array
    {
        return $this->directors;
    }

    public function getPublishedDate(): PublishedDate
    {
        return $this->publishedDate;
    }
}
