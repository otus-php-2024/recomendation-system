<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Parser;

use DateTime;

class PublishedDate
{
    private DateTime $publishedDate;

    public function __construct(DateTime $publishedDate)
    {
        $this->publishedDate = $publishedDate;
    }

    public function getPublishedDate(): DateTime
    {
        return $this->publishedDate;
    }
}
