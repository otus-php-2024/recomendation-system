<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Parser;

use App\Application\Exceptions\ParserException;

class MovieName
{
    private string $name;

    /**
     * @throws ParserException
     */
    public function __construct(string $name)
    {
        $this->validate($name);

        $this->name = $name;
    }

    /**
     * @throws ParserException
     */
    private function validate(string $name): void
    {
        if (empty($name)) {
            throw ParserException::movieNameIsEmpty();
        }
    }

    public function getName(): string
    {
        return $this->name;
    }
}
