<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Parser;

use App\Application\Exceptions\ParserException;

class Trailer
{
    private string $trailer;

    /**
     * @throws ParserException
     */
    public function __construct(string $trailer)
    {
        $this->validate($trailer);

        $this->trailer = $trailer;
    }

    public function getTrailer(): string
    {
        return $this->trailer;
    }

    /**
     * @throws ParserException
     */
    private function validate(string $trailer): void
    {
        if (empty($trailer)) {
            throw ParserException::trailerIsEmpty();
        }

        if (!filter_var($trailer, FILTER_VALIDATE_URL)) {
            throw ParserException::trailerIsNotValidUrl();
        }
    }
}
