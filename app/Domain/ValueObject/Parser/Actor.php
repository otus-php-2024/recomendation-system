<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Parser;

use App\Application\Exceptions\ParserException;

class Actor
{
    private string $name;
    private string $url;

    /**
     * @throws ParserException
     */
    public function __construct(string $name, string $url)
    {
        $this->validate($name, $url);

        $this->name = $name;
        $this->url = $url;
    }

    /**
     * @throws ParserException
     */
    private function validate(string $name, string $url): void
    {
        if (empty($name)) {
            throw ParserException::actorNameIsEmpty();
        }

        if (empty($url)) {
            throw ParserException::actorUrlIsEmpty();
        }

        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw ParserException::actorUrlIsNotValid();
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}
