<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Parser;

use App\Application\Exceptions\ParserException;

class Genre
{
    private string $genre;

    /**
     * @throws ParserException
     */
    public function __construct(string $genre)
    {
        $this->validate($genre);

        $this->genre = $genre;
    }

    public function getGenre(): string
    {
        return $this->genre;
    }

    /**
     * @throws ParserException
     */
    private function validate(string $genre): void
    {
        if (empty($genre)) {
            throw ParserException::genreIsEmpty();
        }
    }
}
