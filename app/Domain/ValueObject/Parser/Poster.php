<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\Parser;

use App\Application\Exceptions\ParserException;

class Poster
{
    private string $poster;

    /**
     * @throws ParserException
     */
    public function __construct(string $poster)
    {
        $this->validate($poster);

        $this->poster = $poster;
    }

    public function getPoster(): string
    {
        return $this->poster;
    }

    /**
     * @throws ParserException
     */
    private function validate(string $poster): void
    {
        if (empty($poster)) {
            throw ParserException::posterIsEmpty();
        }

        if (!filter_var($poster, FILTER_VALIDATE_URL)) {
            throw ParserException::posterIsNotValidUrl($poster);
        }

        if (preg_match('/\.(jpg|png|jpeg)$/', $poster) !== 1) {
            throw ParserException::posterUrlIsNotImage($poster);
        }
    }
}
