<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\AddNewMovie;

use App\AppException;

class PosterStoragePath
{
    private string $path;

    /**
     * @throws AppException
     */
    public function __construct(string $path)
    {
        $this->validate($path);

        $this->path = $path;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @throws AppException
     */
    private function validate(string $path): void
    {
        if (empty($path)) {
            throw AppException::posterStoragePathIsEmpty();
        }
    }
}
