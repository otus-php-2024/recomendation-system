<?php

declare(strict_types=1);

namespace App\Domain\ValueObject\AddNewMovie;

use App\AppException;

class MovieUrl
{
    private string $url;

    /**
     * @throws AppException
     */
    public function __construct(string $url)
    {
        $this->validate($url);

        $this->url = $url;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @throws AppException
     */
    private function validate(string $url): void
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw AppException::movieUrlIsNotValidUrl();
        }
    }
}
