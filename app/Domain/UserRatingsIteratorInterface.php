<?php

namespace App\Domain;

use App\Domain\ValueObject\RecommendationSystem\UserRatings;
use Iterator;

interface UserRatingsIteratorInterface extends Iterator
{
    public function current(): UserRatings;

    public function key(): int;
}
