# Проектная работа

# Установка

## Изолированное окружение

Для развёртывания изолированного окружения пропишите команду
```bash
vagrant up
```

После установки, зайдите в ОС
```bash
vagrant ssh
```

## Подготовка и запуск среды
```bash
cd /vagrant && cp .env.example .env
```

После чего выполните команду

```bash
sudo docker compose up -d
```

Добавьте сайт `mysite.local` в файл `hosts`
```bash
127.0.0.1 mysite.local
```

Готово!

# Использование

Для того, чтобы посмотреть страницу сайта перейдите по ссылке
```text
http://mysite.local/api-docs
```

Дл доступа к консольному приложению выполните команду
```bash
sudo docker exec -it php bash
```

После чего выполните команду
```bash
php artisan
```
Накатываем миграции и запускаем генерацию фейковых данных
```bash
php artisan migrate
php artisan db:seed
```
